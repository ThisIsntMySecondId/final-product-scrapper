const mongoose = require('mongoose');

const List = {
    schema: {
        _id: String,
        merchant_name: String,
        merchant_url: String,
        merchant_id: String,

        categories: Object,
        // categories: [{
        //     categoryName: String,
        //     categoryLink: String,
        //     categorySubLinks: [String],
        // }]
    },
    modelname: 'tempCategories',
}

const ListSchema = new mongoose.Schema(List.schema, { timestamps: true });
const ListModel = mongoose.model(List.modelname, ListSchema);

module.exports = ListModel;
// ? Info Info Model

const mongoose = require('mongoose');

const Info = {
    schema: {
        merchant_name: String,
        merchant_url: String,
        source_url: String,
        merchant_id: String,

        _id: String,
        sku: String,
        model_no: String,

        title: String,
        link: String,
        main_image: String,
        image_gallery: [String],

        cost_price: Number,
        sell_price: Number,
        rating: String,
        reviews: String,
        offers: [String],

        brand: String,
        category: [String],
        key_features: [String],
        color: String,
        hardware_spec: String,

        description: String,
        specifications: [{
            key: String,
            value: String
        }],
        variation: {
            parameter: String,
            variations: [{
                value: String,
                link: String,
            }]
        },

        newProp: String,
    },
    modelname: 'tempinfo2',
}

const InfoSchema = new mongoose.Schema(Info.schema, { timestamps: true });
const InfoModel = mongoose.model(Info.modelname, InfoSchema);

module.exports = InfoModel;
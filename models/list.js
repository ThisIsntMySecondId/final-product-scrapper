// ? Awok Info Model

const mongoose = require('mongoose');

const List = {
    schema: {
        merchant_name: String,
        merchant_url: String,
        source_url: String,
        merchant_id: String,

        _id: String,
        sku: String,
        model_no: String,

        title: String,
        brand: String,
        main_image: String,
        link: String,

        cost_price: Number,
        sell_price: Number,
        rating: String,
        reviews: String,
        discounts: String,

        highlights: [String],
    },
    modelname: 'newTempLists',
}

const ListSchema = new mongoose.Schema(List.schema, { timestamps: true });
const ListModel = mongoose.model(List.modelname, ListSchema);

module.exports = ListModel;
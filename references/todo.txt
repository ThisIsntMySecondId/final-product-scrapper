Things To Do In Scrapper App:
=============================
1. Add scroll delay and page wait delay in list scrappers
2. User cld add configs and models from cli (i.e. can provide custom configs and models to crawler)
3. Add try catch error display in list and static scrappers
4. Add ability to store new merchant at custom place
5. User shd be able to get(scrape) all the product pages instead of hard coding specific page no.
6. Detect and convert relative urls to absolute ones

w. may be refactor list scrapper
x. add delay between crawlings pages and delay after landing to a specific page in dynamic pages. and the delay should be changed by user
y. Refactor most parts of application (where you are storing data in json & csv files,  ...)
z. To rewrite the full application with new architecture
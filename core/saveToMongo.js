const mongoose = require('mongoose');
const config = require('./constants');

const saveToMongo = async (data, model) => {
    console.log(`Savning ${data.length} items to mongodb`);
    const dburl = config.dbUrl;
    try {
        await mongoose.connect(dburl, { useNewUrlParser: true, useUnifiedTopology: true });

        if (Array.isArray(data)) {
            for (obj of data) {
                let res = await model.updateMany({ _id: obj.id }, obj, { upsert: true });
            }
        } else {
            let res = await model.updateMany({ _id: data.id }, data, { upsert: true });
        }

        mongoose.disconnect();
    } catch (err) {
        console.log(err)
    }
}

module.exports = { saveToMongo };
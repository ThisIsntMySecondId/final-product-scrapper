const customScrapper = async (url, config) => {
    return await config.dataExtractorFunction(url);
}

module.exports = { customScrapper };
const infoScrapper = require('./infoScrapper').infoScrapper;
const listScrapper = require('./listScrapper').listScrapper;
const customScrapper = require('./customScrapper').customScrapper;
const saveToMongo = require('./saveToMongo').saveToMongo;

const scrapeAndReturn = async (config, scrapper_mode, url) => {
    return await loadScrapper(scrapper_mode)(url, config);
}

const scrapeAndStore = async (config, model, scrapper_mode, url) => {
    const data = await loadScrapper(scrapper_mode)(url, config);
    console.log('Saving to Mongo Db');
    await saveToMongo(data, model);
    
}

const scrapeWithProfile = async (profile, url) => {
    const data = await loadScrapper(profile.scrapper_mode)(url, profile.config);
    console.log('Saving to Mongo Db');
    await saveToMongo(data, profile.model);
}

const loadScrapper = scrapper_mode => {
    switch (scrapper_mode) {
        case 'info': return infoScrapper;
        case 'list': return listScrapper;
        case 'custom': return customScrapper;
        default: throw new Error("scrapper cannot be found");
    }
}

module.exports = { scrapeAndReturn, scrapeAndStore, scrapeWithProfile };
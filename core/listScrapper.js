const puppeteer = require('puppeteer');
const parse = require('x-ray-parse');
const filters = require('../lib/filters');
const globalProxies = require('./constants').globalProxies;
/*
(required) need container or wrapper of listing items

there are 5 types of product listing page:
	1. static rendered witn normal pagination  ==> selector of next a + limit
	2. dynamic rendered witn normal pagination ==> selector of next a + limit
	4. with ajax pagination                    ==> selector of btn + limit
	3. with load more button to paginate       ==> selector of btn + limit
	5. with infinite scroll                    ==> limit
*/

//TODO: to make it retrieve all products pages when specified

const listScrapper = async (url, config) => {
    switch (config.meta.type) {
        case 'static':
            return await staticList(url, config);
        case 'dynamic':
            return dynamicList(url, config);
        case 'ajax':
            return ajaxList(url, config);
        case 'load_more':
            return loadMoreList(url, config);
        case 'scroll':
            return scrollList(url, config);
        default:
            throw new Error("page or pagination type not found. please mention page or pagination type in config.meta");
    }
}

const parseConfig = (config) => {
    if (config['parsed'] === true)
        return config;
    else {
        for (key in config.dataToFetch) {
            config['dataToFetch'][key] = parse(config.dataToFetch[key]);
        }
        config.meta.pagination_selector = !!config.meta.pagination_selector ? parse(config.meta.pagination_selector) : '';
        config['parsed'] = true;
        return config;
    }
}

const fetchAndProcess = async (wrapper, config, url) => {
    let row = {};   //? row represents one row of fetched data from one wrapper or product container
    for (let key in config.dataToFetch) {
        let { selector: sel, attribute: attr, filters: fils } = config.dataToFetch[key];

        // ? Fetch data
        if (await wrapper.$(sel)) {  //FIXME: if the selector is empty then select the parent element 
            row[key] = attr ? await wrapper.$eval(sel, (elem, attr) => elem.getAttribute(attr), attr) : await wrapper.$eval(sel, (elem) => elem.innerText);
        } else {
            row[key] = '';
        }

        // ? Apply Filters
        if (fils.length > 0) {
            for (fil of fils) {
                if (fil.name in filters) {
                    row[key] = filters[fil.name](row[key]);
                }
            }
        }
    }
    row = { ...row, ...config.staticData };

    // ? Postprocessing
    for (processIndex in config.postProcess) {
        row[processIndex] = config.postProcess[processIndex](row, url);
    }

    return row;
}

async function getPageAndBrowser(config) {
    if (typeof counter === 'undefined') {
        counter = 0;
    }

    if (config.proxies && Array.isArray(config.proxies)) {
        // ? Setting proxy
        let localProxies = config.proxies;
        const proxy = localProxies[counter++ % localProxies.length];
        const proxyUrl = 'http://' + proxy.split('@')[1];
        const proxyUsername = proxy.split(/[\/@:]/g)[3];
        const proxyPwd = proxy.split(/[\/@:]/g)[4];
        
        // ? Launching browser and page
        console.log(`Launching puppeteer with proxy ${proxy} form local proxy specified by config file`);
        const browser = await puppeteer.launch({ args: [`--proxy-server=${proxyUrl}`], headless: false, defaultViewport: null });
        const page = await browser.newPage();
        await page.authenticate({ username: proxyUsername, password: proxyPwd });
        return { page, browser };
    } else if (config.proxies == true) {
        // ? Setting proxy
        const proxy = globalProxies[counter++ % globalProxies.length];
        const proxyUrl = 'http://' + proxy.split('@')[1];
        const proxyUsername = proxy.split(/[\/@:]/g)[3];
        const proxyPwd = proxy.split(/[\/@:]/g)[4];
        
        // ? Launching browser and page
        console.log(`Launching puppeteer with proxy ${proxy} from global proxy specified in constants in core`);
        const browser = await puppeteer.launch({ args: [`--proxy-server=${proxyUrl}`], headless: false, defaultViewport: null });
        const page = await browser.newPage();
        await page.authenticate({ username: proxyUsername, password: proxyPwd });
        return { page, browser };
    } else {
        console.log(`Launching puppeteer`);
        const browser = await puppeteer.launch({headless: false, defaultViewport: null });
        const page = await browser.newPage();
        return { page, browser };
    }


}

const staticList = async (url, config) => {
    let x;
    if (config.proxies && Array.isArray(config.proxies))
        x = require('../lib/xrayInstance')(config.proxies);
    else if (config.proxies === true)
        x = require('../lib/xrayInstance')(globalProxies);
    else
        x = require('../lib/xrayInstance')();

    // ? Fetch data
    let data = await x(url, config.meta.wrapper, [config.dataToFetch]).paginate(config.meta.pagination_selector).limit(config.meta.limit || 1);
    console.log(`Data fetching completed. Total ${data.length} items fetched.`);

    let processedData = [];
    for (row of data) {
        row = { ...row, ...config.staticData };
        // ? Postprocess data
        for (processIndex in config.postProcess) {
            row[processIndex] = config.postProcess[processIndex](row, url);
        }
        processedData.push(row);
    }
    data = processedData;

    return data;
}

const dynamicList = async (url, config) => {
    config = parseConfig(config);
    if (!config.meta.wrapper) throw new Error("wrapper cannot be found");

    // ? init puppeteer, creating browser and opening new page
    const { page, browser } = await getPageAndBrowser(config);
    let data = [];
    let pageNo = 1;
    const limit = config.meta.limit || 1;
    await page.goto(url, { timeout: 0 });

    while (pageNo <= limit) {
        let productWrappers = await page.$$(config.meta.wrapper);
        let dataFromOnePage = [];
        for (wrapper of productWrappers) {

            // TODO: Try if you can shift this loop (productWrappers) in this function
            let row = await fetchAndProcess(wrapper, config, url);

            dataFromOnePage.push(row); //? pushing everything
        }
        data.push(...dataFromOnePage);

        console.log(`Data fetching completed form page ${pageNo}. Total ${productWrappers.length} items fetched`);

        // TODO: refactor this
        if (config.meta.pagination_selector) {
            if (pageNo == limit) break;

            if (!(await page.$(config.meta.pagination_selector.selector))) {
                console.log("pagination_selector link not found. Cannot move to next source");
                break;
            }
            url = await page.$eval(config.meta.pagination_selector.selector, e => e.getAttribute('href'));
            if (config.staticData.merchant_url && !url.includes(config.staticData.merchant_url)) {
                url = config.staticData.merchant_url + url;
            }
            await page.goto(url, { timeout: 0 });
            pageNo++;
        } else {
            console.log("pagination_selector link not found. Cannot move to next source");
            break;
        }
    }

    browser.on('disconnected', () => {
        console.log("Browser closed");
    });
    browser.close();
    return data;
}

const ajaxList = async (url, config) => {
    config = parseConfig(config);
    if (!config.meta.wrapper) throw new Error("wrapper cannot be found");

    // ? init puppeteer, creating browser and opening new page
    const { page, browser } = await getPageAndBrowser(config);

    let data = [];
    let pageNo = 1;
    const limit = config.meta.limit || 1;
    await page.goto(url, { timeout: 0 });

    while (pageNo <= limit) {
        let productWrappers = await page.$$(config.meta.wrapper);
        let dataFromOnePage = [];
        for (wrapper of productWrappers) {

            // TODO: Try if you can shift this loop (productWrappers) in this function
            let row = await fetchAndProcess(wrapper, config, url);

            dataFromOnePage.push(row); //? pushing everything
        }
        data.push(...dataFromOnePage);

        console.log(`Data fetching completed form page ${pageNo}. Total ${productWrappers.length} items fetched`);

        // TODO: refactor this
        if (config.meta.pagination_selector) {
            if (pageNo == limit) break;
            if (!(await page.$(config.meta.pagination_selector.selector))) {
                console.log("pagination_selector link not found. Cannot move to next source");
                break;
            }
            let nextBtn = await page.$(config.meta.pagination_selector.selector);
            await nextBtn.click();
            await page.waitFor(5000);      //FIXME: you dont have to hardcode this value. Because some pages can take more time then 5s to load 
            pageNo++;
        } else {
            console.log("pagination_selector link not found. Cannot move to next source");
            break;
        }
    }

    browser.on('disconnected', () => {
        console.log("Browser closed");
    });
    browser.close();
    return data;
}

const loadMoreList = async (url, config) => {
    config = parseConfig(config);

    // ? init puppeteer, creating browser and opening new page
    const { page, browser } = await getPageAndBrowser(config);
    await page.goto(url);
    await page.waitFor(config.meta.scrollDelay || 3000);

    // ? start clicking load more btn till page limit
    // TODO: limit either by item count or something else
    // FIXME: goes into infinite loop when more item are required then the total no that exists actually on page 
    // eg items on page are 195 and when user asks 200 items
    if (config.meta.limit >= 0) {
        let requiredCount = config.meta.limit || (await page.$$(config.meta.wrapper)).length;
        while ((await page.$$(config.meta.wrapper)).length < requiredCount) {
            await page.click(config.meta.pagination_selector.selector);
            await page.waitFor(config.meta.scrollDelay || 3000);
        }
    } else {
        // ? keep on clicking load more btn untill it exists
        while ((await page.$(config.meta.pagination_selector.selector))) {
            await page.click(config.meta.pagination_selector.selector);
            await page.waitFor(config.meta.scrollDelay || 3000);
        }
    }

    // ? scrape entire data on page and store result
    // ? productWrappers => product containers
    const productWrappers = await page.$$(config.meta.wrapper);

    let data = [];
    for (wrapper of productWrappers) {
        let row = await fetchAndProcess(wrapper, config, url);
        data.push(row);
    }

    // ? close and return
    browser.on('disconnected', () => {
        console.log("Browser closed");
    });
    await browser.close();
    // FIXME: when user requires more items then the items are on the page 
    if (config.meta.limit >= 0) {
        let itemsToBeRemoved = data.length - config.meta.limit;
        data.splice(data.length - itemsToBeRemoved, itemsToBeRemoved);
    }
    console.log(`Data fetching completed. Total ${productWrappers.length} items fetched`);
    return data;
}

const scrollList = async (url, config) => {
    config = parseConfig(config);

    // ? init puppeteer, creating browser and opening new page
    const { page, browser } = await getPageAndBrowser(config);
    await page.goto(url);
    await page.waitFor(config.meta.scrollDelay || 3000);

    // ? start scrolling till page limit
    // TODO: limit either by item count or something else
    // FIXME: goes into infinite loop when more item are required then the total no that exists actually on page 
    // eg items on page are 195 and when user asks 200 items
    if (config.meta.limit >= 0) {
        let requiredCount = config.meta.limit || (await page.$$(config.meta.wrapper)).length;
        while ((await page.$$(config.meta.wrapper)).length < requiredCount) {
            let previousHeight = await page.evaluate('document.body.scrollHeight');
            await page.evaluate("window.scrollBy({top: document.body.scrollHeight, left: 0, behavior: 'smooth'})");
            await page.waitForFunction(`document.body.scrollHeight > ${previousHeight}`);
            await page.waitFor(config.meta.scrollDelay || 3000);
        }
    } else {
        while (await page.evaluate('(window.innerHeight + document.scrollingElement.scrollTop) != document.scrollingElement.scrollHeight')) {
            await page.evaluate("window.scrollBy({top: document.body.scrollHeight, left: 0, behavior: 'smooth'})");
            await page.waitFor(config.meta.scrollDelay || 3000);
        }
    }

    // ? scrape entire data on page and store result
    // ? productWrappers => product containers
    const productWrappers = await page.$$(config.meta.wrapper);

    let data = [];
    for (wrapper of productWrappers) {
        let row = await fetchAndProcess(wrapper, config, url);
        data.push(row);
    }

    // ? close and return
    browser.on('disconnected', () => {
        console.log("Browser closed");
    });
    await browser.close();
    // FIXME: when user requires more items then the items are on the page 
    if (config.meta.limit >= 0) {
        let itemsToBeRemoved = data.length - config.meta.limit;
        data.splice(data.length - itemsToBeRemoved, itemsToBeRemoved);
    }
    console.log(`Data fetching completed. Total ${productWrappers.length} items fetched`);
    return data;
}

module.exports = { listScrapper, staticList, dynamicList, ajaxList, loadMoreList, scrollList };
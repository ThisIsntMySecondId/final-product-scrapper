// ? Modules
const globalProxies = require('./constants').globalProxies;

const categoryScrapper = async (url='', config) => {
    let x;
    if (config.proxies && Array.isArray(config.proxies))
        x = require('../lib/xrayInstance')(config.proxies);
    else if (config.proxies === true)
        x = require('../lib/xrayInstance')(globalProxies);
    else
        x = require('../lib/xrayInstance')();

    // ? Fetch data
    // let data = await x(config.staticData.merchant_url, config.dataToFetch);
    let data = await x(config.staticData.merchant_url, config.dataToFetch.categoryWrapper, [{
        categoryName: config.dataToFetch.categoryName,
        categoryLink: config.dataToFetch.categoryLink,
        categorySubLinks: [config.dataToFetch.categorySubLinks]
    }]);

    // ? Merge data with meta data
    data = { categories: data, ...config.staticData };

    for (processIndex in config.postProcess) {
        data[processIndex] = await config.postProcess[processIndex](data);
    }

    return data;
}

module.exports = { categoryScrapper };
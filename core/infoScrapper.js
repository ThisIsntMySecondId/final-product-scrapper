// ? Modules
const globalProxies = require('./constants').globalProxies;

const infoScrapper = async (url, config) => {
    let x;
    if (config.proxies && Array.isArray(config.proxies))
        x = require('../lib/xrayInstance')(config.proxies);
    else if (config.proxies === true)
        x = require('../lib/xrayInstance')(globalProxies);
    else
        x = require('../lib/xrayInstance')();


    // ? Fetch data
    let data = await x(url, config.dataToFetch);

    // ? Merge data with meta data
    data = { ...data, ...config.staticData };

    // ? Postprocess data
    for (processIndex in config.postProcess) {
        data[processIndex] = await config.postProcess[processIndex](data, url);
    }

    return data;
}

module.exports = { infoScrapper };
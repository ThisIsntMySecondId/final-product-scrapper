const config = require('../Configs/Custom/ubuyInfoCustom');
const model = require('../models/info');

module.exports = {
    config: config,
    model: model,
    scrapper_mode: 'custom'
}
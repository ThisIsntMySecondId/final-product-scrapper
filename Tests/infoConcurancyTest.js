const infoScrapper = require('../core/infoScrapper').infoScrapper;
const jsonFile = require('fs').createWriteStream('./temp.json');
const saveToMongo = require('../core/saveToMongo').saveToMongo;
const model = require('../models/info');

// ? Main
const get = async () => {

    // ? Awok
    const url = 'https://ae.awok.com/mobile-phones/apple_iphone_11_pro_max_6_5_inch_display_512gb_gold/dp-1625121/';
    const config = require('../Configs/Info/awok');
    let data = await infoScrapper(url, config);
    jsonFile.write(JSON.stringify(data));
    require('fs').createWriteStream('temp.html').write(data.description);
    // await saveToMongo(data, model);

}
const get2 = async () => {

    // ?Emaxme
    const url = 'https://www.emaxme.com/s001/catalog/product/view/id/10529/s/iphone-11-pro-max-256gb-space-grey-pre-order-1/category/591/';
    const config = require('../Configs/Info/emaxme');
    let data = await infoScrapper(url, config);
    jsonFile.write(JSON.stringify(data));
    require('fs').createWriteStream('temp.html').write(data.description);
    // await saveToMongo(data, model);

}

get();
get2();
get();
console.log("hello");
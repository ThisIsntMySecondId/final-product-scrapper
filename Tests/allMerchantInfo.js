const infoScrapper = require('../core/infoScrapper').infoScrapper;
const jsonFile = require('fs').createWriteStream('./temp.json');
const saveToMongo = require('../core/saveToMongo').saveToMongo;
const model = require('../models/info');

// ? Main
(async () => {

    // ? Awok
    const url = 'https://ae.awok.com/mobile-phones/apple_iphone_11_pro_max_6_5_inch_display_512gb_gold/dp-1625121/';
    const config = require('../Configs/Info/awok');
    let data = await infoScrapper(url, config);
    jsonFile.write(JSON.stringify(data));
    require('fs').createWriteStream('temp.html').write(data.description);
    // await saveToMongo(data, model);

    // ? Emaxme
    // const url = 'https://www.emaxme.com/s001/catalog/product/view/id/10529/s/iphone-11-pro-max-256gb-space-grey-pre-order-1/category/591/';
    // const config = require('../Configs/Info/emaxme');
    // let data = await infoScrapper(url, config);
    // jsonFile.write(JSON.stringify(data));
    // require('fs').createWriteStream('temp.html').write(data.description);
    // await saveToMongo(data, model);

    // ? Godukan
    // const url = 'https://www.godukkan.com/laptops-computers/laptops/laptop-by-brand/lenovo-234/lenovo-ideapad-v110-amd-15a4-9120-4gb-ram-500gb-15-6-screen-dos-en-black';
    // const url = 'https://www.godukkan.com/mobile-tablet/mobile/apple_mobiles/apple-iphone-11-with-facetime-64gb-4g-lte-red';
    // const config = require('../Configs/Info/godukan');
    // let data = await infoScrapper(url, config);
    // jsonFile.write(JSON.stringify(data));
    // require('fs').createWriteStream('temp.html').write(data.description);
    // await saveToMongo(data, model);

    // ? Jumbo
    // FIXME: id modelno and sku all are same but they have different no for pruduct and another no for product vatiant in the url 
    // const url = 'https://www.jumbo.ae/smart-phones/apple-iphone-11-4g-lte-smartphone/p-0441617-19151979293-cat.html#variant_id=0441617-10553875915';
    // const config = require('../Configs/Info/jumbo');
    // let data = await infoScrapper(url, config);
    // jsonFile.write(JSON.stringify(data));
    // require('fs').createWriteStream('temp.html').write(data.description);
    // await saveToMongo(data, model);

    // ? Letstango
    // const url = 'https://www.letstango.com/apple-iphone-11-pro-max-512gb-gold-without-facetime';
    // const url = 'https://www.letstango.com/lenovo-tab-7-tb-7304i-tablet-7-inch-16gb-1gb-ram-3g-slate';
    // const url = 'https://www.letstango.com/apple-iphone-8-plus-64gb-space-gray?_sgm_campaign=scn_3f07a243fae7e000&_sgm_source=60727&_sgm_action=click';
    // const config = require('../Configs/Info/letstango');
    // let data = await infoScrapper(url, config);
    // jsonFile.write(JSON.stringify(data));
    // require('fs').createWriteStream('temp.html').write(data.description);
    // await saveToMongo(data, model);

    // ? Microless
    // const url = 'https://uae.microless.com/product/apple-iphone-8-plus-64gb-space-gray-with-screen-protector-mq8d2ll-a/';
    // const config = require('../Configs/Info/microless');
    // let data = await infoScrapper(url, config);
    // jsonFile.write(JSON.stringify(data));
    // require('fs').createWriteStream('temp.html').write(data.description);
    // await saveToMongo(data, model);

    // ? Ourshopee
    // const url = 'https://www.ourshopee.com/product-description/Apple-iPhone-4S-Smart-Phone-16GB-3G35inch-iOS-5-Dual-Camera-WiFi-Black/';
    // const config = require('../Configs/Info/ourshopee');
    // let data = await infoScrapper(url, config);
    // jsonFile.write(JSON.stringify(data));
    // require('fs').createWriteStream('temp.html').write(data.description);
    // await saveToMongo(data, model);

    // ? Sharafdg
    //FIXME: description cannot be found 
    // const url = 'https://uae.sharafdg.com/product/samsung-galaxy-note10-256gb-aura-white-sm-n970f-4g-dual-sim-smartphone/';
    // const config = require('../Configs/Info/Sharafdg');
    // let data = await infoScrapper(url, config);
    // jsonFile.write(JSON.stringify(data));
    // require('fs').createWriteStream('temp.html').write(data.description);
    // await saveToMongo(data, model);

    // ? Shopkees
    // const url = 'https://www.shopkees.com/acer-aspire-intelr-coretm-i3-7020u.html';
    // const config = require('../Configs/Info/shopkees');
    // let data = await infoScrapper(url, config);
    // jsonFile.write(JSON.stringify(data));
    // require('fs').createWriteStream('temp.html').write(data.description);
    // await saveToMongo(data, model);

    // ? Sprii
    // const url = 'https://www.sprii.ae/en/kuvings-whole-slow-juicer-c7000-silver.html';
    // const config = require('../Configs/Info/sprii');
    // let data = await infoScrapper(url, config);
    // jsonFile.write(JSON.stringify(data));
    // require('fs').createWriteStream('temp.html').write(data.description);
    // await saveToMongo(data, model);

    // ? Ubuy
    // const url = 'https://www.ubuy.ae/en/search/index/view/product/B07J215RPT/s/samsung-chromebook-plus-v2-2-in-1-4gb-ram-64gb-emmc-13mp-camera-chrome-os-12-2-16-10-aspect-ratio-light-titan-xe520qab-k03us/store/store';
    // const config = require('../Configs/Info/ubuy');
    // let data = await infoScrapper(url, config);
    // jsonFile.write(JSON.stringify(data));
    // jsonFile.write(data.info);
    // await saveToMongo(data, model);


})();
const categoryScrapper = require('../core/categoryScrapper').categoryScrapper;
const model = require('../models/category');
const saveToMongo = require('../core/saveToMongo').saveToMongo;

// const categoryConfig = require('../Merchants/Awok/category');
// const categoryConfig = require('../Merchants/Letstango/category');
const categoryConfig = require('../Merchants/Shopkees/category');


console.log(categoryConfig);

(async () => {
    const data = await categoryScrapper(categoryConfig);
    const jsonFile = require('fs').createWriteStream('./ctemp.json');
    jsonFile.write(JSON.stringify(data));
    await saveToMongo(data, model);
})();
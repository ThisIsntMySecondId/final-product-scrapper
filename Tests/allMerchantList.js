const listScrapper = require('../core/listScrapper').listScrapper;
const jsonFile = require('fs').createWriteStream('./temp.json');
const saveToMongo = require('../core/saveToMongo').saveToMongo;
const model = require('../models/list');

// ? Main
(async () => {

    // FIXME: How are we going to deal with situation where two ids could match for different products from different merchatns 
    // FIXME: How can we display description if we dont have styles from the merchant because some description are just plain html 
    // FIXME: convert all discounts to numbers 
    // FIXME: do we need to convert relative urls in some links to absolute url or merchant url is enough 
    // FIXME: check wether we have sbility to get all the products fro the link provided without specifing the limit 

    // ? Awok
    // FIXME remove dp from id 
    // const url = 'https://ae.awok.com/Laptops-Notebooks/ds-744/';
    // const config = require('../Configs/List/awok');
    // let data = await listScrapper(url, config);
    // jsonFile.write(JSON.stringify(data));
    // await saveToMongo(data, model);

    // ? Emaxme
    // const url = 'https://www.emaxme.com/s001/shop-by-catagory/computer-and-laptop.html';
    // const config = require('../Configs/List/emaxme');
    // let data = await listScrapper(url, config);
    // jsonFile.write(JSON.stringify(data));
    // await saveToMongo(data, model);

    // ? Godukan
    // const url = 'https://www.godukkan.com/laptops-computers/laptops/laptop-by-brand/lenovo-234';
    // const config = require('../Configs/List/godukan');
    // let data = await listScrapper(url, config);
    // jsonFile.write(JSON.stringify(data));
    // await saveToMongo(data, model);

    // ? Jumbo
    // FIXME: ratings and reviews only apper if we scroll down the page 
    // const url = 'https://www.jumbo.ae/mobile-phones/smart-phones#/?stock_check[]=in_stock&make[]=apple&_xhr=1';
    // const url = 'https://www.jumbo.ae/home/search?q=iphone+11';
    // const url = 'https://www.jumbo.ae/home/search?q=iphone+10';
    // const url = 'https://www.jumbo.ae/mobile-phones/smart-phones#/?stock_check[]=in_stock&_xhr=1';
    // const config = require('../Configs/List/jumbo');
    // let data = await listScrapper(url, config);
    // jsonFile.write(JSON.stringify(data));
    // await saveToMongo(data, model);
    
    // ? Letstango
    // const url = 'https://www.letstango.com/?q=dell%20laptops&idx=letsTango_default_products&p=0&is_v=1';
    // const url = 'https://www.letstango.com/mobiles-tablets/mobiles/one-plus';
    // const config = require('../Configs/List/letstango');
    // let data = await listScrapper(url, config);
    // jsonFile.write(JSON.stringify(data));
    // await saveToMongo(data, model);
        
    // ? Microless
    // const url = 'https://uae.microless.com/notebooks/hp/b/l/';
    // const config = require('../Configs/List/microless');
    // let data = await listScrapper(url, config);
    // jsonFile.write(JSON.stringify(data));
    // await saveToMongo(data, model);
        
    // ? Ourshopee
    // const url = 'https://www.ourshopee.com/brands/Apple/5/';
    // const config = require('../Configs/List/ourshopee');
    // let data = await listScrapper(url, config);
    // jsonFile.write(JSON.stringify(data));
    // await saveToMongo(data, model);
        
    // ? Sharafdg
    // const url = 'https://uae.sharafdg.com/?q=iphone%2010&post_type=product';
    // const url = 'https://uae.sharafdg.com/iphone-11/';
    // const url = 'https://uae.sharafdg.com/c/mobiles_tablets/mobiles/';
    // const url = 'https://uae.sharafdg.com/c/mobiles_tablets/mobiles/?dFR[taxonomies.attr.Brand][0]=Oppo&hFR[taxonomies.taxonomies_hierarchical][0]=Mobiles%20%26%20Tablets%20%3E%20Mobiles';
    // const config = require('../Configs/List/sharafdg');
    // let data = await listScrapper(url, config);
    // jsonFile.write(JSON.stringify(data));
    // await saveToMongo(data, model);
        
    // ? Shopkees
    // const url = 'https://www.shopkees.com/catalogsearch/result/?q=laptops';
    // const config = require('../Configs/List/shopkees');
    // let data = await listScrapper(url, config);
    // jsonFile.write(JSON.stringify(data));
    // await saveToMongo(data, model);
        
    // ? Sprii
    // const url = 'https://www.sprii.ae/en/appliances/?p=1';
    // const config = require('../Configs/List/sprii');
    // let data = await listScrapper(url, config);
    // jsonFile.write(JSON.stringify(data));
    // await saveToMongo(data, model);
        
    // ? Ubuy
    // const url = 'https://www.ubuy.ae/en/search/?ref_p=ser_tp&q=laptops&brand=HP';
    // const config = require('../Configs/List/ubuy');
    // let data = await listScrapper(url, config);
    // jsonFile.write(JSON.stringify(data));
    // await saveToMongo(data, model);

})();
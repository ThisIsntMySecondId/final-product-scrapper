// MAIN
(async () => {
    // const url = 'https://www.ubuy.ae/en/search/index/view/product/B07J215RPT/s/samsung-chromebook-plus-v2-2-in-1-4gb-ram-64gb-emmc-13mp-camera-chrome-os-12-2-16-10-aspect-ratio-light-titan-xe520qab-k03us/store/store';
    // const url = 'https://www.ubuy.ae/en/search/index/view/product/B07LGDF34X/s/blu-vivo-xi-6-2-full-hd-smartphone-gsm-unlocked-and-verizon-compatible-128gb-6gb-ram-ai-dual-cameras-silver/store/store';
    // const url = 'https://www.ubuy.ae/en/search/index/view/product/B07R7DY911/s/google-pixel-3a-with-64gb-memory-cell-phone-unlocked-just-black/store/store';
    // const url = 'https://www.ubuy.ae/en/search/index/view/product/B07LB2W9W7/s/smpl-iphone-x-xs-drop-proof-lightweight-protective-wireless-charging-compatible-iphone-case-teal/store/store/kk/dp?ref_p=dp-rp';
    const config = require('../Configs/Custom/ubuyInfoCustom');
    const customScrapper = require('../core/customScrapper').customScrapper;
    let data = await customScrapper(url, config);
    const jsonFile = require('fs').createWriteStream('./temp.json');
    jsonFile.write(JSON.stringify(data));

    const model = require('../models/info');
    const saveToMongo = require('../core/saveToMongo').saveToMongo;
    await saveToMongo(data, model);
})();
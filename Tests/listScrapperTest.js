// 💡 Tests
// ? static
// (async () => {
//     const config = require('../tempconfigs/listStatic');
//     console.log(config);
//     // const url = 'https://www.shopkees.com/catalogsearch/result/?q=laptops';
//     const url = 'https://www.shopkees.com/laptops.html?brand=18';
//     const listScrapper = require('../core/listScrapper').listScrapper;
//     const data = await listScrapper(url, config);
//     console.log(data);
//     console.log(data.length);
//     const saveToMongo = require('../core/saveToMongo').saveToMongo;
//     const model = require('../models/list');
//     await saveToMongo(data, model);
// })();

// ? dynamic
// (async () => {
//     const config = require('../tempconfigs/listDynamic');
//     console.log(config);
//     const url = 'https://www.letstango.com/?q=dell%20laptops&idx=letsTango_default_products&p=0&is_v=1';
//     const listScrapper = require('../core/listScrapper').listScrapper;
//     const data = await listScrapper(url, config);
//     console.log(data);
//     console.log(data.length);
//     const saveToMongo = require('../core/saveToMongo').saveToMongo;
//     const model = require('../models/list');
//     await saveToMongo(data, model);
// })();

// ? ajax
// (async () => {
//     const config = require('../tempconfigs/listAjax');
//     console.log(config);
//     const url = 'https://uae.microless.com/notebooks/asus/b/l/?page=2'
//     const listScrapper = require('../core/listScrapper').listScrapper;
//     const data = await listScrapper(url, config);
//     console.log(data);
//     console.log(data.length);
//     const saveToMongo = require('../core/saveToMongo').saveToMongo;
//     const model = require('../models/list');
//     await saveToMongo(data, model);
// })();

// ? load_more
// (async () => {
//     const config = require('../tempconfigs/listLoadMore');
//     console.log(config);
//     const url = 'https://webscraper.io/test-sites/e-commerce/more/computers/laptops';
//     const listScrapper = require('../core/listScrapper').listScrapper;
//     const data = await listScrapper(url, config);
//     console.log(data);
//     console.log(data.length);
//     const saveToMongo = require('../core/saveToMongo').saveToMongo;
//     const model = require('../models/list');
//     await saveToMongo(data, model);
// })();

// ? scroll
// (async () => {
//     const config = require('../tempconfigs/listScroll');
//     console.log(config);
//     const url = 'https://www.sprii.ae/en/appliances/?p=1';
//     const listScrapper = require('../core/listScrapper').listScrapper;
//     const data = await listScrapper(url, config);
//     console.log(data);
//     console.log(data.length);
//     const saveToMongo = require('../core/saveToMongo').saveToMongo;
//     const model = require('../models/list');
//     await saveToMongo(data, model);
// })();
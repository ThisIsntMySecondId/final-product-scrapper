const listScrapper = require('../core/listScrapper').listScrapper;
const jsonFile = require('fs').createWriteStream('./temp.json');
const saveToMongo = require('../core/saveToMongo').saveToMongo;
const model = require('../models/list');

// const get = async () => {
//     console.log("loading...");
    
//     // ? Awok
//     const url = 'https://ae.awok.com/Laptops-Notebooks/ds-744/';
//     const config = require('../Configs/List/awok');
//     let data = await listScrapper(url, config);
//     jsonFile.write(JSON.stringify(data));
//     await saveToMongo(data, model);
//     console.log("loaded");
// }

const get = async () => {
    console.log("loading...");
    // ? Letstango
    const url = 'https://www.letstango.com/?q=dell%20laptops&idx=letsTango_default_products&p=0&is_v=1';
    const config = require('../Configs/List/letstango');
    let data = await listScrapper(url, config);
    console.log('Writing to json file')
    jsonFile.write(JSON.stringify(data));
    console.log('Saving to moongo')
    await saveToMongo(data, model);
    console.log("loaded");
}

// ? Main
(async () => {
    await get();
    console.log('Hello');
})();
// let obj = {
//     categories: {
//         'All Categories': ['mainUrl', 'aaa', 'bbb', 'ccc'],
//         'Mobiles': ['mainUrl', 'aaa', 'bbb', 'ggg'],
//         'Tv': ['mainUrl', 'aaa', 'bbb', 'ccc', 'ddd'],
//         'Fashion': ['mainUrl', 'ccc', 'fff'],
//         'Furniture': ['mainUrl', 'ccc', 'eee'],
//         'Best Buys': ['mainUrl', 'aaa', 'bbb', 'ccc', 'ddd', 'eee'],
//     },
//     merchant_id: '',
//     merchant_name: 'Letatango',
//     id: 'Letatango',
//     merchant_url: 'https://www.letatango.com',
// };

const save = async (data, model) => {
    console.log(`Savning ${data.length} items to mongodb`);
    const dburl = 'mongodb://localhost:27017/Products';
    try {
        await mongoose.connect(dburl, { useNewUrlParser: true, useUnifiedTopology: true });

        if (Array.isArray(data)) {
            for (obj of data) {
                let res = await model.updateMany({ _id: obj.id }, obj, { upsert: true });
            }
        } else {
            let res = await model.updateMany({ _id: data.id }, data, { upsert: true });
        }

        mongoose.disconnect();
    } catch (err) {
        console.log(err)
    }
}

const mongoose = require('mongoose');

const List = {
    schema: {
        _id: String,
        merchant_name: String,
        merchant_url: String,
        merchant_id: String,
        categories: Object
    },
    modelname: 'tempCategories',
}

const ListSchema = new mongoose.Schema(List.schema, { timestamps: true });
const ListModel = mongoose.model(List.modelname, ListSchema);

const getCategories = async (merchantName, model, category = '') => {
    const dburl = 'mongodb://localhost:27017/Products';
    try {
        await mongoose.connect(dburl, { useNewUrlParser: true, useUnifiedTopology: true });

        categoryList = [];
        let categories = (await model.findOne({ merchant_name: merchantName }, { _id: 0, categories: 1 })).categories;
        mongoose.disconnect();

        if (category) {
            return categories[category];
        } else {
            for (category in categories) {
                categoryList = [...categoryList, ...categories[category]]
            }
            return categoryList;
        }

        // console.log((await model.find({categories:})).categories);       
    } catch (err) {
        console.log(err)
    }
}

(async () => {
    // await save(obj, ListModel);
    console.log((await getCategories('Letstango', ListModel)));
    console.log((await getCategories('Letstango', ListModel)).length);
    console.log(await getCategories('Letstango', ListModel, 'All Categories'));
    console.log((await getCategories('Letstango', ListModel, 'All Categories')).length);
    // console.log(await getCategories('Awok', ListModel, 'Fashion'));
})();
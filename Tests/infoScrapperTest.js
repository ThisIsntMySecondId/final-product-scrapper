// MAIN
(async () => {
    const url = 'https://ae.awok.com/mobile-phones/apple_iphone_11_pro_max_6_5_inch_display_512gb_gold/dp-1625121/';
    const config = require('../tempconfigs/info');
    const infoScrapper = require('../core/infoScrapper').infoScrapper;
    let data = await infoScrapper(url, config);
    const model = require('../models/info');
    const saveToMongo = require('../core/saveToMongo').saveToMongo;
    await saveToMongo(data, model);
})();
// Sharafdg List Config
const sha256 = require('../../lib/utils').sha256;

// ? Master Config Generator
listConfig = {};

listConfig.meta = {
    wrapper: '.product-container .slide',
    type: 'scroll',
    // pagination_selector: '.category-pagination li.active + li a',
    limit: -20,
}
listConfig.proxies = true;
// listConfig.proxies = [
//     'http://pchand02:dbwnZpD6@172.241.234.32:29842'
// ]
listConfig.staticData = {
    merchant_name: 'Sharafdg',
    merchant_url: 'https://uae.sharafdg.com',
    source_url: '',
    merchant_id: ''
}
listConfig.dataToFetch = {
    // id: '.prod-info .add-to-cart a@onclick',
    // sku: '.product-details .product-title | trim',
    // modelNo: '',

    title: 'h4',
    // brand: '',
    link: 'a@href',
    image: 'img@src',
    
    cost_price: '.product-price .cross-price | trim | toFloat',
    sell_price: '.product-price .price  | trim | toFloat',
    ratings: '.rating-badge',
    // reviews: '.testfreaks-items .testfreaks@title',
    discounts: '.discount-holder',

    // highlights: '',
};
listConfig.postProcess = {
    source_url: (data, url) => url,
    id: data => sha256(data["link"]),
};

module.exports = listConfig;
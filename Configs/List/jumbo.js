// ? Jumbo List Config

const xray = require('x-ray');
const filters = require('../../lib/filters');
const x = xray({
    filters: filters
});

// ? Master Config Generator
listConfig = {};

listConfig.meta = {
    wrapper: 'ul#search-result-items li',
    type: 'static',
    pagination_selector: '.summary-response-last .pagination a.next_page@href',
    limit: 2,
}
listConfig.staticData = {
    merchant_name: 'Jumbo',
    merchant_url: 'https://www.jumbo.ae',
    source_url: '',
    merchant_id: ''
}
listConfig.dataToFetch = {
    id: '.add-to-cart form input[name=variant_id]@value',
    // sku: '',
    // modelNo: '',

    title: '.variant-title',
    // brand: '',
    link: '.variant-title a@href',
    image: 'img@src',
    
    cost_price: '.variant-list-price | toFloat',
    sell_price: '.variant-final-price | toFloat',
    ratings: '.testfreaks-items .testfreaks', //? other then first 4 it loads when scrolled down with js and so it may not be retrieved
    reviews: '.testfreaks-items .testfreaks@title', //? other then first 4 it loads when scrolled down with js and so it may not be retrieved
    discounts: '.label-sale',

    // highlights: '',
};
listConfig.postProcess = {
    source_url: (data, url) => url,
};

module.exports = listConfig;
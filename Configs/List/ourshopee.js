// Ourshopee List Config

// ? Master Config Generator
listConfig = {};

listConfig.meta = {
    wrapper: '.single-product',
    type: 'scroll',
    // pagination_selector: '.category-pagination li.active + li a',
    limit: 10,
}
listConfig.staticData = {
    merchant_name: 'Ourshopee',
    merchant_url: 'https://www.ourshopee.com/',
    source_url: '',
    merchant_id: ''
}
listConfig.dataToFetch = {
    id: '.prod-info .add-to-cart a@onclick',
    // sku: '.product-details .product-title | trim',
    // modelNo: '',

    title: '.prod-info h2',
    // brand: '',
    link: '.product-image a@href',
    image: '.product-image a img@src',
    
    cost_price: '.prod-info .price-box .old-price | toFloat',
    sell_price: '.prod-info .price-box .price | toFloat',
    // ratings: '.testfreaks-items .testfreaks',
    // reviews: '.testfreaks-items .testfreaks@title',
    discounts: '.sale-on span',

    // highlights: '',
};
listConfig.postProcess = {
    source_url: (data, url) => url,
    id: data => data["id"].replace(/doCart\(|\);/g, ''),
};

module.exports = listConfig;
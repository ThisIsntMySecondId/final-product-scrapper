// ? Emaxme List Config

// ? Master Config Generator
listConfig = {};

listConfig.meta = {
    wrapper: '.item.product.product-item',
    type: 'static',
    pagination_selector: '.items.pages-items .item.pages-item-next a@href',
    limit: 3,
}
listConfig.proxies = true;
// listConfig.proxies = [
//     'http://pchand02:dbwnZpD6@172.241.234.32:29842'
// ]
listConfig.staticData = {
    merchant_name: 'Emaxme',
    merchant_url: 'https://www.emaxme.com',
    source_url: '',
    merchant_id: ''
}
listConfig.dataToFetch = {
    id: '.product-item-inner .actions-primary form input[name="product"]@value',
    // sku: '',
    // modelNo: '',

    title: '.product.name.product-item-name | trim',
    // brand: '',
    image: '.product.photo.product-item-photo a img@src',
    link: '.product.name.product-item-name a@href',
    
    cost_price: '.price-box.price-final_price > span:nth-child(2) .price | toFloat',
    sell_price: '.price-box.price-final_price > span:nth-child(1) .price | toFloat',
    // ratings: '',
    // reviews: '',
    discounts: '.product.photo.product-item-photo .product-labels .product-label.sale-label',

    // highlights: '',
};
listConfig.postProcess = {
    source_url: (data, url) => url,
};

module.exports = listConfig;
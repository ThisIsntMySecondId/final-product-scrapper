// Ubuy List Config

// ? Master Config Generator
listConfig = {};

listConfig.meta = {
    wrapper: '#usstore-products .product-search',
    type: 'ajax',
    pagination_selector: '.pagination li.active + li a',
    limit: 3,
}
listConfig.staticData = {
    merchant_name: 'Ubuy',
    merchant_url: 'https://www.ubuy.ae',
    source_url: '',
    merchant_id: ''
}
listConfig.dataToFetch = {
    id: '.product-img a span@data-sku',
    // sku: '.product-details .product-title | trim',
    // modelNo: '',

    title: '.product-body h3',
    // brand: '',
    link: '.product-img a@href',
    image: '.product-img a img@data-src',
    
    cost_price: '.product-price .product-old-price | toFloat',
    sell_price: '.product-price | toFloat',
    // ratings: '.testfreaks-items .testfreaks',
    // reviews: '.testfreaks-items .testfreaks@title',
    // discounts: '.product-discount-badge',

    // highlights: '',
};
listConfig.postProcess = {
    source_url: (data, url) => url,
};

module.exports = listConfig;
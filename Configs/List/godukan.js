// ? Emaxme List Config

const xray = require('x-ray');
const filters = require('../../lib/filters');
const x = xray({
    filters: filters
});

// ? Master Config Generator
listConfig = {};

listConfig.meta = {
    wrapper: '.row.main-products.product-grid .product-grid-item',
    type: 'static',
    pagination_selector: '.row.pagination .links ul li.active + li a@href',
    limit: 2,
}
listConfig.staticData = {
    merchant_name: 'Godukan',
    merchant_url: 'https://www.godukkan.com',
    source_url: '',
    merchant_id: ''
}
listConfig.dataToFetch = {
    id: '.product-details .button-group .cart a@onclick',
    // sku: '',
    // modelNo: '',

    title: 'h4',
    // brand: '',
    image: '.product-thumb .image img@data-src',
    link: 'a@href',
    
    temp_price: '.price | trim',
    // cost_price: '.price .price-old | toFloat',
    // sell_price: '.price-box.price-final_price > span:nth-child(1) .price | toFloat',
    // ratings: '',
    // reviews: '',
    discounts: '.label-sale',

    // highlights: '',
};
listConfig.postProcess = {
    source_url: (data, url) => url,
    id: data => data["id"].replace(/addToCart\('|'\);/g,''),
    cost_price: data => data["temp_price"].split(' ')[1] ? filters.toFloat(data["temp_price"].split(' ')[0]) : null,
    sell_price: data => data["temp_price"].split(' ')[1] ? filters.toFloat(data["temp_price"].split(' ')[1]) : filters.toFloat(data["temp_price"].split(' ')[0]),
    temp_price: () => undefined,
};

module.exports = listConfig;
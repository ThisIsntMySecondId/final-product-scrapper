// Microless List Config

// ? Master Config Generator
listConfig = {};

listConfig.meta = {
    wrapper: '.product.grid-list',
    type: 'ajax',
    pagination_selector: '.category-pagination li.active + li a',
    limit: 4,
}
listConfig.staticData = {
    merchant_name: 'Microless',
    merchant_url: 'https://uae.microless.com',
    source_url: '',
    merchant_id: ''
}
listConfig.dataToFetch = {
    id: '.product-details-wrap .product-details .compare-checkbox-container input[name="compare"]@value',
    sku: '.product-details .product-title | trim',
    // modelNo: '',

    title: '.product-details .product-title | trim | upper',
    // brand: '',
    link: '.product-image a@href',
    image: '.product-image img@data-src',
    
    cost_price: '.old-price | toFloat',
    sell_price: '.new-price | toFloat',
    // ratings: '.testfreaks-items .testfreaks',
    // reviews: '.testfreaks-items .testfreaks@title',
    discounts: '.product-discount-badge',

    // highlights: '',
};
listConfig.postProcess = {
    source_url: (data, url) => url,
    sku: (data) => data.sku.split("|").slice(-1).join('').trim(),
};

module.exports = listConfig;
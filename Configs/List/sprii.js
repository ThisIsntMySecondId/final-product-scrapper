// ? Sprii List Config

// ? Master Config Generator
listConfig = {};

listConfig.meta = {
    wrapper: '.item.product.product-item',
    type: 'scroll',
    // pagination_selector: '.category-pagination li.active + li a@href',
    limit: 65,
}
listConfig.staticData = {
    merchant_name: 'Sprii',
    merchant_url: 'https://www.sprii.ae/',
    source_url: '',
    merchant_id: ''
}
listConfig.dataToFetch = {
    id: '.product-item-inner .product-item-actions form input[name="product"]@value',
    sku: '.product-item-inner form@data-product-sku',
    // modelNo: '',

    title: '.product.name.product-item-name | trim',
    // brand: '',
    link: '.product.name.product-item-name a@href',
    image: '.product-image-photo@src',

    sell_price: '.price-box.price-final_price .price-container.price-final_price .price | toFloat',
    cost_price: '.price-box.price-final_price .old-price .price | toFloat',
    // ratings: '',
    // reviews: '',
    discounts: '.pond | trim',

    // highlights: '',
};
listConfig.postProcess = {
    source_url: (data, url) => url,
};

module.exports = listConfig;
// Letstango List Config

const xray = require('x-ray');
const filters = require('../../lib/filters');
const x = xray({
    filters: filters
});
const sha256 = require('../../lib/utils').sha256;

// ? Master Config Generator
listConfig = {};

listConfig.meta = {
    wrapper: '.hits .dealblock_thump',
    type: 'dynamic',
    pagination_selector: '.ais-pagination li.ais-pagination--item__next a',
    limit: 2,
}
listConfig.proxies = true;
// listConfig.proxies = [
//     'http://pchand02:dbwnZpD6@172.241.234.32:29842'
// ]
listConfig.staticData = {
    merchant_name: 'Letstango',
    merchant_url: 'https://www.letstango.com',
    source_url: '',
    merchant_id: ''
}
listConfig.dataToFetch = {
    // id: '.add-to-cart form input[name=variant_id]@value',
    // sku: '',
    // modelNo: '',

    title: 'h1 | capatilize',
    // brand: '',
    link: '.thump_img a@href',
    image: '.thump_img a img@src | ddd',
    
    cost_price: '.oldprice | toFloat',
    sell_price: 'h2 | toFloat',
    // ratings: '.testfreaks-items .testfreaks',
    // reviews: '.testfreaks-items .testfreaks@title',
    // discounts: '.label-sale',

    // highlights: '',
};
listConfig.postProcess = {
    source_url: (data, url) => url,
    id: data => sha256(data["link"]),
};

module.exports = listConfig;
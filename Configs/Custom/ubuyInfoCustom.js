// ? Ubuy Custom Config

const xray = require('x-ray');
const filters = require('../../lib/filters');
const x = xray({
    filters: filters
});
const request = require("request");
const rp = require('request-promise-native');

// ? Master Config Generator
customConfig = {};

customConfig.dataExtractorFunction = async (url) => {
    let finalDataToBeReturned = {};
    let dataContainingScript = await x(url, '#product-view-full + script');
    let vars = dataContainingScript.split(';').slice(0, 13).join(';');
    eval(vars);
    var requestData = {
        selected_asin: selected_asin,
        parent_asin: parent_asin,
        is_custom_id_product: is_custom_id_product,
        storename: storename,
        is_ajax: is_ajax,
        product_id: product_id,
        s_id: s_id,
        lang: lang
    };
    var options = { method: 'POST', url: ajax_url, form: requestData };
    var htmlFromAjaxReq = await rp(options);

    let data = await x(htmlFromAjaxReq, {
        id: 'span#item_id',
        sku: 'input[name="variation_sku"]@value',

        title: 'h2#product-name | trim',
        main_image: '.owl-carousel.owlslider-main li:first-child a img@data-src',
        image_gallery: ['.owl-carousel.owlslider-main li a img@data-src'],

        sell_price: 'span[itemprop="price"]@content | toFloat',
        cost_price: 'h3.product-price .product-old-price | toFloat',
        discount: '.you-save-price .price | trim',
        
        brand: '.brandname span[itemprop="brand"] | trim',
        key_features: ['div[itemprop="description"] li'],
        color: 'none',
        hardware: '',

        description: '#tab2 .row .col-md-12@html',
        specificationAndVariationSource: ['script'],
    });

    data.specificationAndVariationSource = data.specificationAndVariationSource[1].split(/\r\n\r\n/)[0];
    eval(data.specificationAndVariationSource);
    data["variation"] = JSON.parse(translateData.variations.replace(/options/g,'variations').replace(/heading/g,'parameter').replace(/available/g,'link'));
    data["specifications"] = JSON.parse(translateData.specifications.replace(/title/g,'key'));
    data.specificationAndVariationSource = undefined;

    for (variationField of data["variation"]) {
        let variationsArray = [];
        for (vindex in variationField.variations) {
            variationField.variations[vindex].link = vindex;
            variationsArray.push(variationField.variations[vindex]);
        }
        variationField.variations = variationsArray
    }

    finalDataToBeReturned = { ...data };
    finalDataToBeReturned.merchant_name = "Ubuy";
    finalDataToBeReturned.merchant_url = "https://www.ubuy.ae";
    finalDataToBeReturned.source_url = url;
    finalDataToBeReturned.merchant_id = '';


    return finalDataToBeReturned;
}

module.exports = customConfig;
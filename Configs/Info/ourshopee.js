// ? Ourshopee Info Config

const xray = require('x-ray');
const filters = require('../../lib/filters');
const x = xray({
    filters: filters
});

// ? Master Config Generator
infoConfig = {};
infoConfig.meta = {}
infoConfig.staticData = {
    merchant_name: 'Ourshopee',
    merchant_url: 'https://www.ourshopee.com',
    merchant_id: ''
}
infoConfig.dataToFetch = {
    id: 'input[name="pid"]@value',
    sku: '.prod-info tr:first-child td:nth-child(3)',
    model_no: '.prod-info tr:nth-child(3) td:nth-child(3) | trim',

    title: 'h1',
    // link: '',
    main_image: 'meta[property="og:image"]@content',
    image_gallery: ['.ubislider-inner img@src'],

    cost_price: '.prod-info .price-box .price | toFloat',
    sell_price: '.prod-info .price-box .old-price | toFloat',
    ratings: '.rating .rating-container input@value', //? n/a
    reviews: '.rating | trim', //? n/a
    // offers: ['.pdp-icon | trim'],
    discounts: '.sale-on',

    brand: '.prod-info tr:nth-child(2) td:nth-child(3)',
    category: ['.bread-crumb li:not(:first-child):not(:last-Child)'],
    key_features: ['.prod-info table tr | removeInnerSpaces'],
    color: '.extra-dts-panel .row:nth-child(1) > div:nth-child(2) a:not([href])@title',
    hardware: '.extra-dts-panel > .row:nth-child(2) > div:nth-child(2) a:not([href])',

    description: '#pr-description p:first-child',
    specifications: x('#pr-description table tr', [{
        key: 'td:nth-child(1)',
        value: 'td:nth-child(2)'
    }]),
    variation: {
        color: x('.extra-dts-panel > .row:nth-child(1) > div:nth-child(2) a', [{
            value: '@title',
            link: '@href'
        }]),
        storage: x('.extra-dts-panel > .row:nth-child(2) > div:nth-child(2) a', [{
            value: '',
            link: '@href'
        }]),
    },
};
infoConfig.postProcess = {
    link: (_, url) => url,
    variation: data => {
        let newVariations = [];
        for (variationIndex in data["variation"]) {
            let variationObj = {};
            variationObj["parameter"] = variationIndex;
            variationObj["variations"] = data["variation"][variationIndex];
            newVariations.push(variationObj);
        }
        return newVariations;
    }
};

module.exports = infoConfig;
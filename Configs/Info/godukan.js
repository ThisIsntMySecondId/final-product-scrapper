// ? Godukan Info Config

const xray = require('x-ray');
const x = xray({
    filters: {
        trim: str => str.trim(),
    }
});

// ? Master Config Generator
infoConfig = {};
infoConfig.meta = {}
infoConfig.staticData = {
    merchant_name: 'Godukan',
    merchant_url: 'https://www.godukkan.com',
    merchant_id: ''
}
infoConfig.dataToFetch = {
    id: 'input[name="product_id"]@value',
    sku: 'meta[itemprop="sku"]@content',
    model_no: 'ul.description span.p-model',

    title: 'h1.heading-title | trim',
    // link: '',
    main_image: '#image@src',
    image_gallery: ['#product-gallery a@href'],

    cost_price: '.price .price-old | toFloat',
    sell_price: 'meta[itemprop="price"]@content | toFloat', //? alternative .price .price-new but it will not give price on products with only one price
    // ratings: { rating: '.product-rating-count', reviews: '.reviewCount span | trim' },
    // reviews: { rating: '.product-rating-count', reviews: '.reviewCount span | trim' },
    // offers: ['.pdp-icon | trim'],
    // discount: '',

    brand: 'ul.description li.p-brand a',
    category: ['ul.breadcrumb li:not(:first-child):not(:last-Child)'],
    // key_features: ['.product.attribute.overview li'],
    // color: '.pd_info_colors a.selected',
    // hardware: '.pd_info_storage a.selected',

    description: '#tab-description@html',
    specifications: x('#tab-specification table tr tr', [{
        key: 'td:nth-child(1)',
        value: 'td:nth-child(2)'
    }]),
    // variation: {
    //     storage: x('.pd_info_storage a', [{
    //         // value: ' | capatilize',   //FIXME: this does not work because capatilise filter that is defined in filters file is not available because it uses context of filters that are defined at the top 
    //         value: '',
    //         link: '@href'
    //     }]),
    //     colors: x('.pd_info_colors a', [{
    //         value: '',
    //         link: '@href'
    //     }]),
    // },
};
infoConfig.postProcess = {
    link: (data, url) => url,
};

module.exports = infoConfig;
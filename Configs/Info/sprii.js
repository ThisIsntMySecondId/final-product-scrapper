// ? Sprii Info Config

const sha256 = require('../../lib/utils').sha256;
const xray = require('x-ray');
const filters = require('../../lib/filters');
const x = xray({
    filters: filters
});

// ? Master Config Generator
infoConfig = {};
infoConfig.meta = {}
infoConfig.staticData = {
    merchant_name: 'Sprii',
    merchant_url: 'https://www.sprii.ae',
    merchant_id: ''
}
infoConfig.dataToFetch = {
    // id: 'script[type="application/ld+json"]',
    temp_id: '#block-product-request script[type="text/x-magento-init"]',
    sku: 'td[data-th]',
    // model_no: '.product-title .prod-extra p span | trim',

    title: 'h1 | trim',
    // link: '',
    main_image: '.fotorama__stage__frame.fotorama__active img@src',
    image_gallery: ['.fotorama__nav__shaft img@src'],
    temp_image_gallery: ['script[type="text/x-magento-init"]'],

    cost_price: '.old-price | trim | toFloat',
    sell_price: '.price-box.price-final_price | trim | toFloat',
    // ratings: '.product-info-main .product-reviews-summary .rating-summary span[itemprop="bestRating"]',
    // reviews: '.product-info-main .product-reviews-summary .reviews-actions span[itemprop="reviewCount"] | trim',
    // offers: ['.pdp-icon | trim'], //? not technically offers
    discounts: '#youSaveWrapper span:nth-child(3)',

    brand: 'td[data-th="Brand"]',
    category: ['.breadcrumbs li:not(:first-child):not(:last-Child) | trim'],
    key_features: x('table tr', [{
        key: 'th.label',
        value: 'td.data',
    }]),
    // color: '.nav.features li:last-child .h-content',
    // hardware: '.extra-dts-panel > .row:nth-child(2) > div:nth-child(2) a:not([href])',

    description: '.description@html',
    // specifications: x('.additional-attributes-wrapper.table-wrapper tbody tr', [{
    //     key: '.col.label',
    //     value: '.col.data'
    // }]),
    // variation: x('.alternative-slider .slide', [{
    //     product_id: '',
    //     value: 'img@title',
    //     link: 'a@href',
    // }])
};
infoConfig.postProcess = {
    link: (_, url) => url,
    id: data => JSON.parse(data["temp_id"])['*']['Magento_Ui/js/core/app']['components']['productRequest']['config']['settings']['product_id'],
    temp_id: () => undefined,
    image_gallery: data => JSON.parse(data["temp_image_gallery"][6])['[data-gallery-role=gallery-placeholder]']['mage/gallery/gallery']['data'].map(d => d.img),
    temp_image_gallery: () => undefined,
    key_features: data => {
        let newkeyFeatures = [];
        for (feature of data["key_features"]) 
            newkeyFeatures.push(`${feature.key} : ${feature.value}`);
        return newkeyFeatures;
    }


};

module.exports = infoConfig;
// ? Jumbo Info Config

const xray = require('x-ray');
const filters = require('../../lib/filters');
const x = xray({
    filters: filters
});

// ? Master Config Generator
infoConfig = {};
infoConfig.meta = {}
infoConfig.staticData = {
    merchant_name: 'Jumbo',
    merchant_url: 'https://www.jumbo.ae/',
    merchant_id: ''
}
infoConfig.dataToFetch = {
    //? product id and variant id is in url
    id: 'meta[itemprop="productID"]@content | trim',
    sku: 'meta[itemprop="sku"]@content | trim',
    model_no: 'span[itemprop="model"] | trim',

    title: 'h1 | trim',
    // link: '',
    main_image: '#catalog-images img@src',
    image_gallery: ['.pdp-thumbnail img@src'],

    cost_price: '.list_price.strike | toFloat',
    sell_price: '.our_price | trim | toFloat',
    ratings: '.tf-rating | trim', //? n/a
    reviews: '.tf-count | trim', //? n/a
    // offers: ['.pdp-icon | trim'],
    // discounts: '.product-price-wrapper .discounts .off-badge',

    // brand: 'ul.description li.p-brand a',
    category: ['.bread-crumbs div:not(:first-child)'],
    // key_features: ['.short-description-block ul li'],
    color: '.catalog-options ul:nth-child(4) li.selected .catalog-option-title', //? n/a
    hardware: '.catalog-options ul:nth-child(2) li.selected .catalog-option-title', //? n/a

    description: '.pdp-tabs .tab-pane #description@html',
    specifications: x('#feature_groups table tr', [{
        key: ':not(.feature_name)',
        value: '.feature_name'
    }]),
    variation: x('.catalog-options', {
        storage: ['ul:nth-child(2) li .catalog-option-title'],
        colors: ['ul:nth-child(4) li .catalog-option-title'],
    }),
};
infoConfig.postProcess = {
    link: (_, url) => url,
};

module.exports = infoConfig;
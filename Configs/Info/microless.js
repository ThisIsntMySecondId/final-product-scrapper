// ? Microless Info Config

const xray = require('x-ray');
const filters = require('../../lib/filters');
const x = xray({
    filters: filters
});

// ? Master Config Generator
infoConfig = {};
infoConfig.meta = {}
infoConfig.staticData = {
    merchant_name: 'Microless',
    merchant_url: 'https://uae.microless.com',
    merchant_id: ''
}
infoConfig.dataToFetch = {
    id: 'input[name="product_id"]@value',
    sku: '.product-additional-info .sku | trim',
    // model_no: 'ul.description span.p-model',

    title: 'h1.product-title-h1 | trim',
    // link: '',
    main_image: 'meta[property="og:image"]@content',
    image_gallery: ['#product-images-slider img@src'],

    cost_price: '.product-price-wrapper .product-price-old .amount-fig | toFloat',
    sell_price: '.product-price-wrapper .product-price | toFloat',
    // ratings: { rating: '.product-rating-count', reviews: '.reviewCount span | trim' },
    // reviews: { rating: '.product-rating-count', reviews: '.reviewCount span | trim' },
    // offers: ['.pdp-icon | trim'],
    discounts: '.product-price-wrapper .discounts .off-badge',

    // brand: 'ul.description li.p-brand a',
    category: ['.breadcrumb li:not(:first-child):not(:last-child) | trim'],
    // key_features: ['.short-description-block ul li'],
    color: '.product-variants-container > .wrap-variants:nth-child(1) .variant-option.selected | removeInnerSpaces',
    hardware: '.product-variants-container > .wrap-variants:nth-child(2) .variant-option.selected | removeInnerSpaces',

    description: '.product-description@html | trim',
    specifications: x('.product-attribute-row', [{
        key: '.col-md-3 | trim',
        value: '.col-md-9 | trim'
    }]),
    variation: x('.product-variants-container .wrap-variants', [{
        parameter: 'div[style="padding: 14px 0 7px 0; font-weight: bold;"] | trim',
        variations: x('.variant-option', [{
            value: ' | removeInnerSpaces',
            link: '@href'
        }])
    }]),
};
infoConfig.postProcess = {
    link: (_, url) => url,
    variation: data => {
        let newVariation = [];
        for (variation of data["variation"]) {
            variation.parameter = variation.parameter.replace(/Select|:/g, '');
            newVariation.push(variation);
        }
        return newVariation;
    }
};

module.exports = infoConfig;
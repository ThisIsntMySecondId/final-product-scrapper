// ? Sharafdg Info Config

const xray = require('x-ray');
const filters = require('../../lib/filters');
const x = xray({
    filters: filters
});

// ? Master Config Generator
infoConfig = {};
infoConfig.meta = {}
infoConfig.staticData = {
    merchant_name: 'Sharafdg',
    merchant_url: 'https://uae.sharafdg.com',
    merchant_id: ''
}
infoConfig.dataToFetch = {
    id: '.add_to_cart_button@data-product_id',
    sku: '.product-title .prod-extra:nth-child(4) p span | trim',
    model_no: '.product-title .prod-extra p span | trim',

    title: 'h1 | trim | upper',
    // link: '',
    main_image: '.mainproduct-slider img@src',
    image_gallery: ['.slider.slider-nav .mpt-item .sdg-ratio img@src'],

    cost_price: '.cross-price .strike | toFloat',
    sell_price: '.total--sale-price | toFloat',
    ratings: '.product-rating-count',
    reviews: '.reviewCount span | trim',
    offers: ['.pdp-icon | trim'], //? not technically offers
    discounts: '.product-disc span',

    // brand: '.prod-info tr:nth-child(2) td:nth-child(3)',
    category: ['.other:not(:last-child)'],
    key_features: x('.clearfix.nav.features li', [{
        key: 'strong',
        value: '.h-content',
        desc: '.tooltip@html'
    }]),
    color: '.nav.features li:last-child .h-content',
    // hardware: '.extra-dts-panel > .row:nth-child(2) > div:nth-child(2) a:not([href])',

    description: '#inpage_container@html',
    specifications: x('.detailspecbox table table tr', [{
        //// groupTitle: 'th',
        //// specs: x('tr:not(:first-child)', [{
            key: 'td.specs-heading',
            value: 'td:not(.specs-heading)'
        ////}])
    }]),
    variation: x('.alternative-slider .slide', [{
        product_id: '',
        value: 'img@title',
        link: 'a@href',
    }])
};
infoConfig.postProcess = {
    link: (_, url) => url,
    key_features: data => {
        let newKeyFeatures = [];
        for (feature of data["key_features"]) {
            let str = `${feature.key} : ${feature.value} (${feature.desc})`;
            newKeyFeatures.push(str);
        }
        return newKeyFeatures;
    }
};

module.exports = infoConfig;
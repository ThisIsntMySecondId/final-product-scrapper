// ? Emaxme Info Config

// ? Master Config Generator
infoConfig = {};
infoConfig.meta = {}
// infoConfig.proxies = true;
infoConfig.proxies = [
    'http://pchand02:dbwnZpD6@172.241.234.32:29842'
]
const x = require('../../lib/xrayInstance')();
infoConfig.staticData = {
    merchant_name: 'Emaxme',
    merchant_url: 'https://www.emaxme.com',
    merchant_id: ''
}
infoConfig.dataToFetch = {
    id: '.product-info-main .product-info-stock-sku div[itemprop="sku"]',
    // sku: '.prod_detail_cart_info .buy_now_btn a@data-pid | trim',
    model_no: '.product-info-main .product-info-stock-sku div[itemprop="model_mob "]',

    title: 'h1.page-title | trim',
    // link: '',
    main_image: 'meta[property="og:image"]@content',
    image_gallery: ['.fotorama__nav__shaft img@src'],
    temp_image_gallery: ['script[type="text/x-magento-init"]'],
    cost_price: '.price-box.price-final_price > span:nth-child(2) .price | toFloat',
    sell_price: '.price-box.price-final_price > span:nth-child(1) .price | toFloat',
    // ratings: { rating: '.product-rating-count', reviews: '.reviewCount span | trim' },
    // reviews: { rating: '.product-rating-count', reviews: '.reviewCount span | trim' },
    // offers: ['.pdp-icon | trim'],
    // discount: '',

    // brand: '',
    // category: ['.page_breadcrums_box ul li[itemtype]:not(:first-child)'],
    key_features: ['.product.attribute.overview li'],
    // color: '.pd_info_colors a.selected',
    // hardware: '.pd_info_storage a.selected',

    description: '.product.attribute.description@html',
    specifications: x('.data.table.additional-attributes tbody tr', [{
        key: 'th',
        value: 'td'
    }]),
    // variation: {
    //     storage: x('.pd_info_storage a', [{
    //         // value: ' | capatilize',   //FIXME: this does not work because capatilise filter that is defined in filters file is not available because it uses context of filters that are defined at the top 
    //         value: '',
    //         link: '@href'
    //     }]),
    //     colors: x('.pd_info_colors a', [{
    //         value: '',
    //         link: '@href'
    //     }]),
    // },
};
infoConfig.postProcess = {
    link: (data, url) => url,
    image_gallery: data => JSON.parse(data["temp_image_gallery"][6])['[data-gallery-role=gallery-placeholder]']['mage/gallery/gallery']['data'].map(d => d.img),
    temp_image_gallery: () => undefined,
};

module.exports = infoConfig;
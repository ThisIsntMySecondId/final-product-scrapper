// ? Awok Info Config

const xray = require('x-ray');
const x = xray({
    filters: {
        trim: str => str.trim(),
    }
});

// ? Master Config Generator
infoConfig = {};
infoConfig.meta = {}
infoConfig.staticData = {
    merchant_name: 'Awok',
    merchant_url: 'https://ae.awok.com',
    source_url: '',
    merchant_id: ''
}
infoConfig.dataToFetch = {
    id: '.prod_detail_cart_info .buy_now_btn a@data-pid | trim',
    sku: '.prod_detail_cart_info .buy_now_btn a@data-pid | trim',
    model_no: '',

    title: 'h1 | trim',
    // link: '',
    main_image: '.prod_preview .main img@src',
    image_gallery: ['.owl-controls .owl-dots .owl-dot@data-src'],

    cost_price: '.prod_detail_cart_prices .old_price | toFloat',
    sell_price: '.prod_detail_cart_prices .new_price | toFloat',
    // ratings: { rating: '.product-rating-count', reviews: '.reviewCount span | trim' },
    // reviews: { rating: '.product-rating-count', reviews: '.reviewCount span | trim' },
    // offers: ['.pdp-icon | trim'],
    // discount: '',

    // brand: '',
    category: ['.page_breadcrums_box ul li[itemtype]:not(:first-child)'],
    //? Key feature will be array of strings
    // key_features: x('.clearfix.nav.features li', [{
    //     key: 'strong',
    //     value: '.h-content',
    //     desc: '.tooltip@html'
    // }]),
    color: '.pd_info_colors a.selected',
    hardware: '.pd_info_storage a.selected',

    description: '.product_content_wrapper #tab2 .product_block@html',
    specifications: x('.product_content_wrapper #tab2 UL.product_specs li', [{
        key: 'li > b',
        value: 'li > span'
    }]),
    variation: {
        storage: x('.pd_info_storage a', [{
            // value: ' | capatilize',   //FIXME: this does not work because capatilise filter that is defined in filters file is not available because it uses context of filters that are defined at the top 
            value: '',
            link: '@href'
        }]),
        colors: x('.pd_info_colors a', [{
            value: '',
            link: '@href'
        }]),
    },
};
infoConfig.postProcess = {
    link: (data = "", url) => url,
    // id: (data, url = "") => data["link"].split('/')[5],
    newProp: data => `id => ${data["id"]} color: ${data["color"]} hardware: ${data["hardware"]}`,
    variation: (data) => {
        let variations = [];
        for (index in data.variation) {
            let variation = {};
            variation["parameter"] = index;
            variation["variations"] = data.variation[index];
            variations.push(variation);
        }
        return variations;
    },
    // sku: () => undefined,
};

module.exports = infoConfig;
// // ? Shopkees List Config

// // ? id in '.product-item-inner .product-item-actions form input[name="product"]@value'

// const shopkeesConfig = {
//     merchant: 'https://www.shopkees.com/',
//     scope: '.item.product-item',
//     mainData: {
//         id: '.product-item-inner .product-item-actions form input[name="product"]@value',
//         title: '.product-item-details strong | trim',
//         cost_price: '.price-box.price-final_price > span:nth-child(2) .price | trim',
//         sell_price: '.price-box.price-final_price > span:nth-child(1) .price | trim',
//         link: 'a@href',
//         image: 'img@src',
//      },
//     nextPage: '.items.pages-items .item.pages-item-next a@href',
//     limit: 3,
//     type: 'list',
//     mode: 'xray',
// }
// module.exports = shopkeesConfig;

// ? Shopkees Info Config

const xray = require('x-ray');
const x = xray({
    filters: {
        trim: str => str.trim(),
    }
});

// ? Master Config Generator
listConfig = {};

listConfig.meta = {
    wrapper: '.item.product-item', //? the container for individual list items
    type: 'static',  //? type of page and the pagination over all : static, dynamic, ajax, load_more, scroll
    pagination_selector: '.items.pages-items .item.pages-item-next a@href',   //? selector of button for load more or ajax pagination or selector of a for static or dynamic pages, empty for infinite scroll
    limit: 3,
}
listConfig.staticData = {
    merchant_name: 'Shopkees',
    merchant_url: 'https://www.shopkees.com/',
    source_url: '',
    merchant_id: ''
}
listConfig.dataToFetch = {
    id: '.product-item-inner .product-item-actions form input[name="product"]@value',
    // sku: '',
    // modelNo: '',

    title: '.product-item-details strong | trim',
    // brand: '',
    image: 'img@src',
    link: 'a@href',
    
    cost_price: '.price-box.price-final_price > span:nth-child(2) .price | toFloat',
    sell_price: '.price-box.price-final_price > span:nth-child(1) .price | toFloat',
    // ratings: '',
    // reviews: '',
    // discounts: '',

    // highlights: '',
};
listConfig.postProcess = {
    source_url: (data, url) => url,
    brand: (data) => data["link"].split('/')[3].split('-').slice(0, 1).map(str => str[0].toUpperCase() + str.slice(1)).join(' '),
    discounts: (data) => data["brand"] + ' 30% Free',
};

module.exports = listConfig;
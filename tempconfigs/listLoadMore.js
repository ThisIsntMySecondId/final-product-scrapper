// ? Load More

// ? Master Config Generator
listConfig = {};

listConfig.meta = {
    wrapper: '.thumbnail',
    type: 'load_more',
    pagination_selector: '.ecomerce-items-scroll-more',
    limit: 32,
}
listConfig.staticData = {
    merchant_name: 'Webscraper',
    merchant_url: 'https://webscraper.io',
    source_url: '',
    merchant_id: ''
}
listConfig.dataToFetch = {
    // id: '.product-item-inner .product-item-actions form input[name="product"]@value',
    // sku: '.product-item-inner form@data-product-sku',
    // modelNo: '',

    title: 'a.title | trim',
    // brand: '',
    link: 'a.title@href',
    image: '.img-responsive@src',

    sell_price: '.pull-right.price',
    // cost_price: '.price-box.price-final_price .old-price .price | toFloat',
    ratings: '.ratings',
    // reviews: '',
    // discounts: '.pond',

    highlights: 'p.description',
};
listConfig.postProcess = {
    source_url: (data, url) => url,
    link: data => data.merchant_url + data.link,
    id: data => data.link.split('/')[7],
    sell_price: data => parseFloat(data.sell_price.replace('$','')),
};

module.exports = listConfig;
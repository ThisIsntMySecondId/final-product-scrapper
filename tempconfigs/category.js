// ? Letstango Category Config
// category config also uses list scrapper

// ? Master Config Generator
categoryConfig = {};
categoryConfig.meta = {}
// categoryConfig.proxies = true;
// categoryConfig.proxies = [
//     'http://pchand02:dbwnZpD6@172.241.234.32:29842'
// ]
const x = require('../../lib/xrayInstance')();
categoryConfig.staticData = {
    merchant_name: 'Letstango',
    merchant_url: 'https://www.letstango.com',
    merchant_id: ''
}
categoryConfig.dataToFetch = {
    // categories: x('.navbar-nav li.nav-item', [{
    //     name: 'a.ico',
    //     link: 'a.ico@href',
    //     subLinks: ['a@href']
    // }]),
    categoryWrapper: '.navbar-nav li.nav-item',     //? The container of names of main categories
    categoryName: 'a.nav-link',              //? The name of main categories
    categoryLink: 'a.nav-link@href',                //? the link of main categories
    categorySubLinks: '.megamenu a',                //? all the links under the main category
};
categoryConfig.postProcess = {
};

module.exports = categoryConfig;
// ? ajax

// ? Microless List Config

// ? id in '@data-prodid' and 
// ? '.product-details-wrap .product-details .compare-checkbox-container input[name="compare"]@value'
// note there is also the sku of product in '.product-details-wrap .product-details .attribute-list ul li' which matches the products sku on info page only on search page

// for sku from product category page you need to extract it from the name the last entity after |

// ? Master Config Generator
listConfig = {};

listConfig.meta = {
    wrapper: '.product.grid-list', //? the container for individual list items
    type: 'ajax',  //? type of page and the pagination over all : static, dynamic, ajax, load_more, scroll
    pagination_selector: '.category-pagination li.active + li a',   //? selector of button for load more or ajax pagination or selector of a for static or dynamic pages, empty for infinite scroll
    limit: 3,
}
listConfig.staticData = {
    merchant_name: 'Microless',
    merchant_url: 'https://uae.microless.com',
    source_url: '',
    merchant_id: ''
}
listConfig.dataToFetch = {
    id: '.product-details-wrap .product-details .compare-checkbox-container input[name="compare"]@value',
    sku: '.product-details .product-title | trim',
    // modelNo: '',

    title: '.product-details .product-title | trim',
    // brand: '',
    link: '.product-image a@href',
    image: '.product-image img@data-src',

    sell_price: '.new-price | toFloat',
    cost_price: '.old-price | toFloat',
    // ratings: '',
    // reviews: '',
    discounts: '.product-discount-badge',

    // highlights: '',
};
listConfig.postProcess = {
    source_url: (data, url) => url,
    sku: (data) => data.sku.split("|").slice(-1).join('').trim(),
};

module.exports = listConfig;
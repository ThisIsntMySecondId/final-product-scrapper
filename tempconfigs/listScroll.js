// ? scroll

// ? sprii List Config

// ? Master Config Generator
listConfig = {};

listConfig.meta = {
    wrapper: '.item.product.product-item', //? the container for individual list items
    type: 'scroll',  //? type of page and the pagination over all : static, dynamic, ajax, load_more, scroll
    // pagination_selector: '.category-pagination li.active + li a@href',   //? selector of button for load more or ajax pagination or selector of a for static or dynamic pages, empty for infinite scroll
    limit: 64,
}
listConfig.staticData = {
    merchant_name: 'Sprii',
    merchant_url: 'https://www.sprii.ae/',
    source_url: '',
    merchant_id: ''
}
listConfig.dataToFetch = {
    id: '.product-item-inner .product-item-actions form input[name="product"]@value',
    sku: '.product-item-inner form@data-product-sku',
    // modelNo: '',

    title: '.product.name.product-item-name | trim',
    // brand: '',
    link: '.product.name.product-item-name a@href',
    image: '.product-image-photo@src',

    sell_price: '.price-box.price-final_price .price-container.price-final_price .price | toFloat',
    cost_price: '.price-box.price-final_price .old-price .price | toFloat',
    // ratings: '',
    // reviews: '',
    discounts: '.pond',

    // highlights: '',
};
listConfig.postProcess = {
    source_url: (data, url) => url,
};

module.exports = listConfig;
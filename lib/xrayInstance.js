const xray = require('x-ray');
const makeDriver = require('request-x-ray');
const filters = require('../lib/filters');

let count = 0;

module.exports = (proxies = '') => {
    if (proxies && Array.isArray(proxies) && proxies.length > 0) {
        //? return x-ray function x with proxies
        const options = {
            method: "GET",
            proxy: proxies[count++ % proxies.length]
        }
        const driver = makeDriver(options);
        const x = xray({
            filters: filters,
        });
        x.driver(driver);
        return x;
    } else {
        //? return x-ray function without proxies
        const x = xray({
            filters: filters
        });
        return x;
    }
}
const filters = {
    trim: str => str.trim(),
    upper: str => str.toUpperCase(),
    capatilize: x => x.toUpperCase(),
    default: (x, str) => {
        if (x.length === 0) return str;
    },
    toFloat: str => str ? parseFloat(str.replace(/[a-zA-Z,:()%-]/g, '').trim()) : null,
    removeInnerSpaces: str => str.trim().split('\n').map(s => s.trim()).filter(s => s.length).join(' '),
}

module.exports = filters;
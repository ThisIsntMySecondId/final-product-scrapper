const categoryScrapper = require('../core/categoryScrapper').categoryScrapper;
const infoScrapper = require('../core/infoScrapper').infoScrapper;
const listScrapper = require('../core/listScrapper').listScrapper;
const customScrapper = require('../core/customScrapper').customScrapper;

const concurency = require('../core/constants').defaultConcurrency
const crawler = async (config, scrapper_mode, urls) => {
    let finalResults = [];
    let noi = parseInt(urls.length / concurency) + 1; //? No. of iterations
    for (i = 0; i < noi; i++) {
        let promiseArray = [];
        for (j = 0; j < concurency; j++) {
            if (urls[concurency * i + j]) {
                console.log(urls[concurency * i + j])
                console.log('url => ', concurency * i + j)
                promiseArray.push(loadScrapper(scrapper_mode)(urls[concurency * i + j], config));
            }
        }
        let iterationResult = await Promise.all(promiseArray);
        finalResults = [...finalResults, ...iterationResult];
    }
    
    if (Array.isArray(finalResults[0])) {
        let flattenedFinalResults = [];
        for (subArray of finalResults) {
            flattenedFinalResults = [...flattenedFinalResults, ...subArray];
        }
        finalResults = flattenedFinalResults;
    }
    return finalResults;

}

const loadScrapper = scrapper_mode => {
    switch (scrapper_mode) {
        case 'category': return categoryScrapper;
        case 'list': return listScrapper;
        case 'info': return infoScrapper;
        case 'custom': return customScrapper;
        default: throw new Error("scrapper cannot be found");
    }
}

module.exports = { crawler };

// ? Main test
(async () => {
    // ? Info Urls
    // let urls = [
    //     'https://www.letstango.com/apple-watch-series-4-gps-44mm-space-gray-aluminum-case-with-black-sport-band',
    //     'https://www.letstango.com/vtech-star-wars-stormtrooper-camera-smartwatch-black',
    //     'https://www.letstango.com/apple-watch-series-4-gps-44mm-space-gray-aluminum-case-with-black-sport-band?_sgm_campaign=scn_3f07a243fae7e000&_sgm_source=93825&_sgm_action=click',
    //     'https://www.letstango.com/huawei-band-3-pro-blue',
    //     'https://www.letstango.com/polar-vantage-m-advanced-running-multisport-watch-w/-gps-wrist-based-heart-rate-black-m/l',
    //     'https://www.letstango.com/apple-watch-series-4-gps-44mm-space-gray-aluminum-case-with-black-sport-loop',
    //     'https://www.letstango.com/huawei-gt-smartwatch-black',
    //     'https://www.letstango.com/huawei-talk-band-b5-smart-band-black',
    //     'https://www.letstango.com/smart-2030-w-007-smart-watch-android-os-single-sim-128m?_sgm_campaign=scn_3f07a243fae7e000&_sgm_source=77579&_sgm_action=click',
    //     'https://www.letstango.com/samsung-galaxy-watch-42mm-sm-r810-midnight-black-2018',
    //     'https://www.letstango.com/samsung-gear-sport-smartwatch-black',
    //     'https://www.letstango.com/huawei-band-3-pro-gold',
    //     'https://www.letstango.com/samsung-galaxy-watch-42mm-sm-r810-rose-gold-2018',
    // ];

    // ? Dynamic List urls
    // let urls = [
    //     "https://www.letstango.com/mobiles-tablets/mobiles",
    //     "https://www.letstango.com/mobiles-tablets/mobiles",
    //     "https://www.letstango.com/mobiles-tablets/mobiles/apple",
    //     "https://www.letstango.com/mobiles-tablets/mobiles/samsung",
    //     "https://www.letstango.com/mobiles-tablets/mobiles/huawei",
    //     "https://www.letstango.com/mobiles-tablets/mobiles/google-pixel",
    //     "https://www.letstango.com/mobiles-tablets/mobiles/one-plus",
    //     "https://www.letstango.com/mobiles-tablets/mobiles/xiaomi",
    //     "https://www.letstango.com/mobiles-tablets/mobiles/nokia",
    //     "https://www.letstango.com/mobiles-tablets/mobiles/sony",
    //     "https://www.letstango.com/mobiles-tablets/mobiles/blackberry",
    // ];

    // ? Static List urls
    // let urls = [
    //     "https://www.shopkees.com/computers-in-dubai/desktop-in-dubai.html",
    //     "https://www.shopkees.com/computers-in-dubai/desktop-in-dubai.html?cat=205",
    //     "https://www.shopkees.com/computers-in-dubai/desktop-in-dubai.html?cat=207",
    //     "https://www.shopkees.com/computers-in-dubai/desktop-in-dubai.html?cat=206",
    //     "https://www.shopkees.com/computers-in-dubai/desktop-in-dubai.html?cat=241",
    //     "https://www.shopkees.com/computers-in-dubai/desktop-in-dubai.html?cat=295",
    //     "https://www.shopkees.com/computers-in-dubai/desktop-in-dubai.html?cat=205",
    //     "https://www.shopkees.com/computers-in-dubai/desktop-in-dubai.html?cat=207",
    //     "https://www.shopkees.com/computers-in-dubai/desktop-in-dubai.html?cat=206",
    //     "https://www.shopkees.com/computers-in-dubai/desktop-in-dubai.html?cat=241",
    //     "https://www.shopkees.com/computers-in-dubai/desktop-in-dubai.html?cat=295",
    // ];

    // ? Static List urls
    // let urls = [
    //     "https://ae.awok.com/mobile-and-accessories/ds-581/",
    //     "https://ae.awok.com/mobile-and-accessories/ds-581/",
    //     "https://ae.awok.com/Mobile-Phones/ds-582/",
    //     "https://ae.awok.com/mobile-phones/ds-582/",
    //     "https://ae.awok.com/mobile-phones/ds-582/",
    //     "https://ae.awok.com/smart-watches/ds-1038/?keywords=Watch+Mobile&set-filter=Y",
    //     "https://ae.awok.com/mobile-phones/ds-582/?operating-system=android&set-filter=Y",
    //     "https://ae.awok.com/mobile-phones/ds-582/?brand=apple&set-filter=Y",
    //     "https://ae.awok.com/mobile-phones/ds-582/?brand=samsung&set-filter=Y",
    //     "https://ae.awok.com/Mobile-Phones/ds-582/?operating-system=windows,windows-phone&set-filter=Y",
    // ];

    // ? Scroll List urls
    // let urls = [
    //     "https://uae.sharafdg.com/c/mobiles_tablets/mobiles/",  //? =306
    //     "https://uae.sharafdg.com/c/mobiles_tablets/tablets/",  //? =109
    //     "https://uae.sharafdg.com/accessories-to-buy/",         //? =321
    //     "https://uae.sharafdg.com/audio-and-video/",            //? =539
    //     "https://uae.sharafdg.com/c/computing/laptops/",        //? =206
    // ];

    // // const letstangoConfig = require('../Merchants/Letstango/info');
    // // const letstangoConfig = require('../Merchants/Letstango/listFromListingPage');
    // // const letstangoConfig = require('../Configs/List/letstango');
    // const letstangoConfig = require('../Merchants/Shopkees/list');
    // // const letstangoConfig = require('../Merchants/Awok/list');
    // const letstangoConfig = require('../Configs/List/sharafdg');
    // const letstangoConfig = require('../Merchants/Letstango/category');

    // console.log(letstangoConfig);
    // let results = await crawler(letstangoConfig, 'list', urls);
    // urls = [letstangoConfig.staticData.merchant_url]
    // let results = await crawler(letstangoConfig, 'category', urls);
    // const fs = require('fs').createWriteStream('letstangoInfos.json').write(JSON.stringify(results));
})();
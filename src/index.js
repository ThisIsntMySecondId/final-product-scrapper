const fs = require('fs');
const path = require('path');
const { save, getListUrls, getInfoUrls } = require('./db');
const { crawler } = require('./crawler');

// TODO: make external configs to be used when specified
// TODO: make external models availalble

const createNewMerchant = async (merchantName) => {

    console.log(`creating merchant ${merchantName}`);

    // FIXME: Fix the relative address 
    if (fs.existsSync(`./Merchants/${merchantName}`)) {
        console.log('Merchant already exists');
        process.exit();
    }

    fs.mkdirSync(`./Merchants/${merchantName}`);
    fs.copyFileSync('./src/initialConfigs/category.js', `./Merchants/${merchantName}/category.js`);
    fs.copyFileSync('./src/initialConfigs/info.js', `./Merchants/${merchantName}/info.js`);
    fs.copyFileSync('./src/initialConfigs/list.js', `./Merchants/${merchantName}/list.js`);
    console.log(`${merchantName} created successfully \nPlease fill the configs information in the Merchant/${merchantName} directory.`);
}

const store = async (merchantName, what, urls, category, isJson, isCsv) => {
    console.log('merchantName : ', merchantName);
    console.log('what : ', what);
    console.log('urls : ', urls);
    console.log('category : ', category);
    console.log('isJson : ', isJson);
    console.log('isCsv : ', isCsv);
    try {
        // Fixme: you need to use ../ to require and other stuffs and ./ to copy and other stuffs 
        console.log(merchantName);

        const categoryConfig = require(`../Merchants/${merchantName}/category.js`);
        const listConfig = require(`../Merchants/${merchantName}/list`);
        const infoConfig = require(`../Merchants/${merchantName}/info`);

        const categoryModel = require('../models/category');
        const listModel = require('../models/list');
        const infoModel = require('../models/info');

        switch (what) {
            case 'cats': {
                let results;
                let categoryModel = require('../models/category');
                console.log('Getting url from category config');
                console.log('retrieving category list');
                results = await crawler(categoryConfig, 'category', [categoryConfig.staticData.merchant_url]);

                if (isJson) {
                    console.log('Stroing to Json file : ', isJson);
                    // FIXME: why we need to initialize fs when we have initialized fs at top 
                    const fs = require('fs').createWriteStream(isJson).write(JSON.stringify(results));
                    console.log('File stored at path : ' + path.resolve(isJson))
                    return;
                }

                // TODO: make categories to be stored in csv file
                if (isCsv) {
                    console.log('Stroing to Csv file : ', isCsv);
                    console.log(path.resolve(isCsv))
                    return;
                }

                console.log('storing to mongo db');
                await save(results, categoryModel);
                console.log('saved successfully to mongo');
                break;
            }
            case 'list': {
                let results;
                let listModel = require('../models/list');
                if (typeof category === 'undefined' && urls) {
                    console.log("Blahblah")
                    // TODO: To make this work i.e. to make the file containing urls exists and get urls from the file
                    //? The file would be json file
                    console.log('urlList : ', path.resolve(urls));
                    // FIXME: why we need to initialize fs when we have initialized fs at top 
                    const fs = require('fs');
                    if (!fs.existsSync(urls)) {
                        console.log('file doesnt exist. Please check the file name again');
                        return;
                    }
                    let urlsArr;
                    try {
                        urlsArr = JSON.parse(fs.readFileSync(urls, 'utf8'));
                    } catch (e) {
                        if (e instanceof SyntaxError)
                            console.log("please make sure that your json file is perfect");
                        return;
                    }
                    console.log(urlsArr.urls);
                    console.log('Getting results from the above specified urls');
                    console.log(listConfig);
                    results = await crawler(listConfig, 'list', urlsArr.urls);

                    if (isJson) {
                        console.log('Stroing to Json file : ', isJson);
                        // FIXME: why we need to initialize fs when we have initialized fs at top 
                        const fs = require('fs').createWriteStream(isJson).write(JSON.stringify(results));
                        console.log('File stored at path : ' + path.resolve(isJson));
                        console.log(`Total ${results.length} items saved`);
                        return;
                    }

                    // TODO: make categories to be stored in csv file
                    if (isCsv) {
                        console.log('Stroing to Csv file : ', isCsv);
                        console.log(path.resolve(isCsv))
                        return;
                    }

                    console.log('storing to mongo db');
                    await save(results, listModel);
                    console.log('saved successfully to mongo');
                    return;
                } else if (category) {
                    console.log('Getting list urls from ' + category + ' category of ' + merchantName + ' merchant from mongo db');
                    urlList = await getListUrls(merchantName, category, categoryModel);
                    console.log(urlList);
                    console.log(urlList.length);

                    results = await crawler(listConfig, 'list', urlList);

                    if (isJson) {
                        console.log('Stroing to Json file : ', isJson);
                        // FIXME: why we need to initialize fs when we have initialized fs at top 
                        const fs = require('fs').createWriteStream(isJson).write(JSON.stringify(results));
                        console.log('File stored at path : ' + path.resolve(isJson));
                        console.log(`Total ${results.length} items saved`);
                        return;
                    }

                    // TODO: make categories to be stored in csv file
                    if (isCsv) {
                        console.log('Stroing to Csv file : ', isCsv);
                        console.log(path.resolve(isCsv))
                        return;
                    }

                    console.log('storing to mongo db');
                    await save(results, listModel);
                    console.log('saved successfully to mongo');
                    return;
                } else {
                    console.log('Getting all list urls of ' + merchantName + ' from mongo db');
                    urlList = await getListUrls(merchantName, category, categoryModel);
                    console.log(urlList);
                    console.log(urlList.length);

                    results = await crawler(listConfig, 'list', urlList);

                    if (isJson) {
                        console.log('Stroing to Json file : ', isJson);
                        // FIXME: why we need to initialize fs when we have initialized fs at top 
                        const fs = require('fs').createWriteStream(isJson).write(JSON.stringify(results));
                        console.log('File stored at path : ' + path.resolve(isJson));
                        console.log(`Total ${results.length} items saved`);
                        return;
                    }

                    // TODO: make categories to be stored in csv file
                    if (isCsv) {
                        console.log('Stroing to Csv file : ', isCsv);
                        console.log(path.resolve(isCsv))
                        return;
                    }

                    console.log('storing to mongo db');
                    await save(results, listModel);
                    console.log('saved successfully to mongo');
                    return;
                }
            }
            case 'info': {
                let results;
                let infoModel = require('../models/info');
                if (urls) {
                    //? The file would be json file
                    console.log('urlList : ', path.resolve(urls));
                    // FIXME: why we need to initialize fs when we have initialized fs at top 
                    const fs = require('fs');
                    if (!fs.existsSync(urls)) {
                        console.log('file doesnt exist. Please check the file name again');
                        return;
                    }
                    let urlsArr;
                    try {
                        urlsArr = JSON.parse(fs.readFileSync(urls, 'utf8'));
                    } catch (e) {
                        if (e instanceof SyntaxError)
                            console.log("please make sure that your json file is perfect");
                        return;
                    }
                    console.log(urlsArr.urls);
                    console.log('Getting results from the above specified urls');
                    results = await crawler(infoConfig, 'info', urlsArr.urls);

                    if (isJson) {
                        console.log('Stroing to Json file : ', isJson);
                        // FIXME: why we need to initialize fs when we have initialized fs at top 
                        const fs = require('fs').createWriteStream(isJson).write(JSON.stringify(results));
                        console.log('File stored at path : ' + path.resolve(isJson));
                        console.log(`Total ${results.length} items saved`);
                        return;
                    }

                    // TODO: make categories to be stored in csv file
                    if (isCsv) {
                        console.log('Stroing to Csv file : ', isCsv);
                        console.log(path.resolve(isCsv))
                        return;
                    }

                    console.log('storing to mongo db');
                    await save(results, infoModel);
                    console.log('saved successfully to mongo');
                    return;
                } else {
                    console.log('Getting all info urls from list collection of ' + merchantName + ' merchant mongo db');
                    

                    urlList = await getInfoUrls(merchantName, category, listModel);
                    console.log(urlList);
                    console.log(urlList.length);

                    results = await crawler(infoConfig, 'info', urlList);

                    if (isJson) {
                        console.log('Stroing to Json file : ', isJson);
                        // FIXME: why we need to initialize fs when we have initialized fs at top 
                        const fs = require('fs').createWriteStream(isJson).write(JSON.stringify(results));
                        console.log('File stored at path : ' + path.resolve(isJson));
                        console.log(`Total ${results.length} items saved`);
                        return;
                    }

                    // TODO: make categories to be stored in csv file
                    if (isCsv) {
                        console.log('Stroing to Csv file : ', isCsv);
                        console.log(path.resolve(isCsv))
                        return;
                    }

                    console.log('storing to mongo db');
                    await save(results, infoModel);
                    console.log('saved successfully to mongo');
                    return;
                }
            }
            default: console.log('what is undefined. It has to be either cats or list or info');
        }
    } catch (err) {
        if (err.code === 'MODULE_NOT_FOUND')
            console.log('Cant find specific merchant. Please make sure that you have created the provided merchant.');
        console.log(err)
    }
}

module.exports = { createNewMerchant, store };
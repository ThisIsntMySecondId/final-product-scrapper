// ? dynamic

// ? Letstango Info Config
const sha = require('../lib/utils').sha256;


// ? Master Config Generator
listConfig = {};

listConfig.meta = {
    wrapper: '.hits .dealblock_thump', //? the container for individual list items
    type: 'dynamic',  //? type of page and the pagination over all : static, dynamic, ajax, load_more, scroll
    pagination_selector: '.ais-pagination li.ais-pagination--item__next a@href',   //? selector of button for load more or ajax pagination or selector of a for static or dynamic pages, empty for infinite scroll
    // pagination_selector: '',   //? selector of button for load more or ajax pagination or selector of a for static or dynamic pages, empty for infinite scroll
    limit: 3,
}
listConfig.staticData = {
    merchant_name: 'Letstango',
    merchant_url: 'https://www.letstango.com',
    source_url: '',
    merchant_id: ''
}
listConfig.dataToFetch = {
    // id: '.product-item-inner .product-item-actions form input[name="product"]@value',
    // sku: '',
    // modelNo: '',

    title: 'h1',
    // brand: '',
    link: '.thump_img a@href',
    image: '.thump_img a img@src',

    sell_price: 'h2 | toFloat',
    cost_price: '.oldprice | toFloat',
    // ratings: '',
    // reviews: '',
    // discounts: '',

    // highlights: '',
};
listConfig.postProcess = {
    source_url: (data, url) => url,
    id: (data) => sha(data["link"]),
};

module.exports = listConfig;
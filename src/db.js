// db.templists.aggregate([{$match: {'merchant_name': 'Awok'}}, {$group : {_id : '$merchant_name', urls: {$push: '$link'}}}])

const mongoose = require('mongoose');
const config = require('../core/constants');

const save = async (data, model) => {
    console.log(`Savning ${data.length} items to mongodb`);
    const dburl = config.dbUrl;
    try {
        await mongoose.connect(dburl, { useNewUrlParser: true, useUnifiedTopology: true });

        if (Array.isArray(data)) {
            for (obj of data) {
                let res = await model.updateMany({ _id: obj.id }, obj, { upsert: true });
            }
        } else {
            let res = await model.updateMany({ _id: data.id }, data, { upsert: true });
        }

        mongoose.disconnect();
    } catch (err) {
        console.log(err)
    }
}

const getListUrls = async (merchantName, category = '', model) => {
    const dburl = 'mongodb://localhost:27017/Products';
    try {
        await mongoose.connect(dburl, { useNewUrlParser: true, useUnifiedTopology: true });

        categoryList = [];
        let categories = (await model.findOne({ merchant_name: merchantName }, { _id: 0, categories: 1 })).categories;
        mongoose.disconnect();

        if (category) {
            return categories[category];
        } else {
            for (category in categories) {
                categoryList = [...categoryList, ...categories[category]]
            }
            return categoryList;
        }
    } catch (err) {
        console.log(err)
    }
}

const getInfoUrls = async (merchantName, category = '', model) => {
    const dburl = 'mongodb://localhost:27017/Products';
    try {
        await mongoose.connect(dburl, { useNewUrlParser: true, useUnifiedTopology: true });

        productList = [];
        let productLinks = (await model.find({ merchant_name: merchantName }, { _id: 0, link: 1 }));
        mongoose.disconnect();
        
        // ? because the db will give u array of object containing single key-value pair for link
        let flattenedProductLinks = [];
        for (productLink of productLinks) {
            flattenedProductLinks.push(productLink.link);
        }
        
        return flattenedProductLinks;
    } catch (err) {
        console.log(err)
    }

}

module.exports = { save, getListUrls, getInfoUrls };
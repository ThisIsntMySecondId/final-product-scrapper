const program = require('commander');
program.version('0.0.1');

const core = require('./core');
const { createNewMerchant, store } = require('./src');
// ? node scrapperApp profile url                       =>4
// ? node scrapperApp config scrappng_mode url          =>5
// ? node scrapperApp config model scrapping_mode url   =>6

// // ? Main
// (async () => {
//     if (process.argv.length === 4) {
//         const profile = require(process.argv[2]);
//         const url = process.argv[3];
//         await core.scrapeWithProfile(profile, url);
//     } else if (process.argv.length === 5) {
//         const config = require(process.argv[2]);
//         const scrapping_mode = process.argv[3];
//         const url = process.argv[4];
//         console.log(await core.scrapeAndReturn(config, scrapping_mode, url));
//     } else if (process.argv.length === 6) {
//         const config = require(process.argv[2]);
//         const model = require(process.argv[3]);
//         const scrapping_mode = process.argv[4];
//         const url = process.argv[5];
//         await core.scrapeAndStore(config, model, scrapping_mode, url);
//     } else {
//         console.log("Innsufficient arguments provided");
//     }
// })();


program
    .command('new <merchantName>')
    .description('Creates new merchant folder with necessary configs')
    .action(merchantName => {
        createNewMerchant(merchantName);
    });

program
    .command('store <merchantName> <what> [urls]')
    .description('Stores the Merchants data either to mongo or json or csv. What refers to either Category, List or Info and Urls is if you wants to retrieve results from spicified list of urls')
    .option('--json <filename>', 'Store data in provided json file')
    .option('--csv <filename>', 'Store data in provided csv file')
    .option('--category <categoryname>', 'Specific category for scrapping list')
    .action((merchantName, what,  urls, args) => store(merchantName, what, urls, args.category, args.json, args.csv));

program.parse(process.argv);
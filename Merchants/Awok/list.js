// ? Awok List Config

// ? Master Config Generator
listConfig = {};

listConfig.meta = {
    wrapper: '.productslist_item',
    type: 'static',
    pagination_selector: '.modern-page-navigation a.modern-page-next@href',
    limit: 3,
}
listConfig.proxies = true;
// listConfig.proxies = [
//     'http://pchand02:dbwnZpD6@172.241.234.32:29842'
// ]
listConfig.staticData = {
    merchant_name: 'Awok',
    merchant_url: 'https://ae.awok.com/',
    source_url: '',
    merchant_id: ''
}
listConfig.dataToFetch = {
    // id: '.product-item-inner .product-item-actions form input[name="product"]@value',
    // sku: '',
    // modelNo: '',

    title: '.productslist_item_image img@title',
    // brand: '',
    image: '.productslist_item_image img@src',
    link: 'a.productslist_item_link@href',
    
    cost_price: '.productslist_item_priceold | toFloat',
    sell_price: '.productslist_item_pricenew | toFloat',
    // ratings: '',
    // reviews: '',
    discounts: '.productslist_item_banner',

    // highlights: '',
};
listConfig.postProcess = {
    source_url: (data, url) => url,
    id: data => data["link"].split('/')[5]
};

module.exports = listConfig;
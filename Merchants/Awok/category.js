// ? Awok Category Config
// category config also uses list scrapper

// ? Master Config Generator
categoryConfig = {};
categoryConfig.meta = {}
// categoryConfig.proxies = true;
// categoryConfig.proxies = [
//     'http://pchand02:dbwnZpD6@172.241.234.32:29842'
// ]
const x = require('../../lib/xrayInstance')();
categoryConfig.staticData = {
    merchant_name: 'Awok',
    merchant_url: 'https://ae.awok.com',
    id: 'Awok',
    merchant_id: ''
}
categoryConfig.dataToFetch = {
    // categories: x('.site_main_menu .menu_content > ul > li', [{
    //     name: 'a.ico',
    //     link: 'a.ico@href',
    //     subLinks: ['a@href']
    // }]),
    categoryWrapper: '.site_main_menu .menu_content > ul > li',
    categoryName: 'a.ico',
    categoryLink: 'a.ico@href',
    categorySubLinks: 'a@href',
};
categoryConfig.postProcess = {
    categories: data => {
        let newCategories = {};

        for (category of data['categories']) {
            if(category['categoryName'])
                newCategories[category['categoryName']] = [category['categoryLink'], ...category['categorySubLinks']];
        }

        return newCategories;
    }
};

module.exports = categoryConfig;
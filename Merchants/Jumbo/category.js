// ? Jumbo Category Config

// ? Master Config Generator
categoryConfig = {};
categoryConfig.meta = {}
// categoryConfig.proxies = true;
// categoryConfig.proxies = [
//     'http://pchand02:dbwnZpD6@172.241.234.32:29842'
// ]
const x = require('../../lib/xrayInstance')();
categoryConfig.staticData = {
    merchant_name: 'Jumbo',
    merchant_url: 'https://www.jumbo.ae',
    merchant_id: ''
}
categoryConfig.dataToFetch = {
    // categories: x('.navbar-nav li.nav-item', [{
    //     name: 'a.ico',
    //     link: 'a.ico@href',
    //     subLinks: ['a@href']
    // }]),
    categoryWrapper: '.navbar-nav li.dropdown',
    categoryName: 'a.dropdown-toggle',
    categoryLink: 'a.dropdown-toggle@href',
    categorySubLinks: 'a@href',
};
categoryConfig.postProcess = {
    categories: data => {
        let newCategories = {};

        for (category of data['categories']) {
            if (category['categoryName'])
                newCategories[category['categoryName']] = [...new Set([category['categoryLink'], ...category['categorySubLinks']].filter(a => a.length))];
                // ? the above merges both main link and sub links in to an array and then filters them removing duplicates and empty values
        }

        return newCategories;
    },
};

module.exports = categoryConfig;
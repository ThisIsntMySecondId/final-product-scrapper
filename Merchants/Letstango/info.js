// ? Letstango Info Config

const xray = require('x-ray');
const x = xray({
    filters: {
        trim: str => str.trim(),
    }
});

// ? Master Config Generator
infoConfig = {};
infoConfig.meta = {}
infoConfig.staticData = {
    merchant_name: 'Letstango',
    merchant_url: 'https://www.letstango.com',
    merchant_id: ''
}
infoConfig.dataToFetch = {
    id_sku_source: 'link[rel="manifest"] + script + script',
    // sku: 'meta[itemprop="sku"]@content',
    // model_no: 'ul.description span.p-model',

    title: 'h1 | trim',
    // link: '',
    main_image: '.product-gallery img@src',
    image_gallery: ['.product-gallery-nav img@src'],

    cost_price: '.product-price-block .oldprice | toFloat',
    sell_price: '.product-price-block .selling-price | toFloat',
    // ratings: { rating: '.product-rating-count', reviews: '.reviewCount span | trim' },
    // reviews: { rating: '.product-rating-count', reviews: '.reviewCount span | trim' },
    // offers: ['.pdp-icon | trim'],
    discounts: '.product-price-block .percentage | trim',

    // brand: 'ul.description li.p-brand a',
    category: ['.pagecrumb li:not(:first-child):not(:last-child) | trim'],
    key_features: ['.short-description-block ul li'],
    color: '.dropdown-container button | trim',
    hardware: '.dropdown-container:nth-child(2) button | trim',

    description: '.overview@html',
    specifications: x('.table-row', [{
        key: '.table-cell.title',
        value: '.table-cell.content | trim',
    }]),
    variation: x('.dropdown-container', [{
        parameter: 'p | trim',
        variations: x('ul li', [{
            value: 'a',
            link: 'a@href'
        }])
    }]),
};
infoConfig.postProcess = {
    link: (data, url) => url,
    id: data => {
        eval(data["id_sku_source"].split('dataLayer.push')[0])
        return dataLayer[0].productId;
    },
    sku: data => {
        eval(data["id_sku_source"].split('dataLayer.push')[0])
        return dataLayer[0].productSku;
    },
    id_sku_source: () => undefined,
};

module.exports = infoConfig;
// ? Letstango Category Config
// category config also uses list scrapper

// ? Master Config Generator
categoryConfig = {};
categoryConfig.meta = {}
// categoryConfig.proxies = true;
// categoryConfig.proxies = [
//     'http://pchand02:dbwnZpD6@172.241.234.32:29842'
// ]
const x = require('../../lib/xrayInstance')();
categoryConfig.staticData = {
    merchant_name: 'Letstango',
    merchant_url: 'https://www.letstango.com',
    id: 'Letstango',
    merchant_id: ''
}
categoryConfig.dataToFetch = {
    // categories: x('.navbar-nav li.nav-item', [{
    //     name: 'a.ico',
    //     link: 'a.ico@href',
    //     subLinks: ['a@href']
    // }]),
    categoryWrapper: '.navbar-nav li.nav-item',     //? The container of names of main categories
    categoryName: 'a.nav-link',                     //? The name of main categories
    categoryLink: 'a.nav-link@href',                //? the link of main categories
    categorySubLinks: '.megamenu a@href',           //? all the links under the main category
};
categoryConfig.postProcess = {
    categories: data => {
        let newCategories = {};

        for (category of data['categories']) {
            if (category['categoryName'])
                newCategories[category['categoryName']] = [category['categoryLink'], ...category['categorySubLinks']];
        }

        return newCategories;
    }
};

module.exports = categoryConfig;
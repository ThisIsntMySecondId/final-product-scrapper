// ? Shopkees List Config

// ? Master Config Generator
listConfig = {};

listConfig.meta = {
    wrapper: '.item.product-item',
    type: 'static',
    pagination_selector: '.items.pages-items .item.pages-item-next a@href',
    limit: 3,
}
listConfig.staticData = {
    merchant_name: 'Shopkees',
    merchant_url: 'https://www.shopkees.com/',
    source_url: '',
    merchant_id: ''
}
listConfig.dataToFetch = {
    id: '.product-item-inner .product-item-actions form input[name="product"]@value',
    // sku: '',
    // modelNo: '',

    title: '.product-item-details strong | trim',
    // brand: '',
    image: 'img@src',
    link: 'a@href',
    
    cost_price: '.price-box.price-final_price > span:nth-child(2) .price | trim | toFloat',
    sell_price: '.price-box.price-final_price > span:nth-child(1) .price | trim | toFloat',
    // ratings: '',
    // reviews: '',
    // discounts: '.product.photo.product-item-photo .product-labels .product-label.sale-label',

    // highlights: '',
};
listConfig.postProcess = {
    source_url: (data, url) => url,
};

module.exports = listConfig;
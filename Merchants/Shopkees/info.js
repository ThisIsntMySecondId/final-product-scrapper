// ? Shopkees Info Config

const sha256 = require('../../lib/utils').sha256;
const xray = require('x-ray');
const filters = require('../../lib/filters');
const x = xray({
    filters: filters
});

// ? Master Config Generator
infoConfig = {};
infoConfig.meta = {}
infoConfig.staticData = {
    merchant_name: 'Shopkees',
    merchant_url: 'https://www.shopkees.com',
    merchant_id: ''
}
infoConfig.dataToFetch = {
    sku: 'script[type="application/ld+json"]',
    // id: '.product-title .prod-extra:nth-child(4) p span | trim',
    // model_no: '.product-title .prod-extra p span | trim',

    title: '.product-info-main h1.page-title | trim',
    // link: '',
    main_image: '.fotorama__stage__frame.fotorama__active img@src',
    image_gallery: ['.fotorama__nav__shaft img@src'],
    temp_image_gallery: ['script[type="text/x-magento-init"]'],
    
    cost_price: '.product-info-main .price-box.price-final_price > span:nth-child(2) .price | toFloat',
    sell_price: '.product-info-main .price-box.price-final_price > span:nth-child(1) .price | toFloat',
    ratings: '.product-info-main .product-reviews-summary .rating-summary span[itemprop="bestRating"]',
    reviews: '.product-info-main .product-reviews-summary .reviews-actions span[itemprop="reviewCount"] | trim',
    // offers: ['.pdp-icon | trim'], //? not technically offers
    // discounts: '.product-disc span',

    // brand: '.prod-info tr:nth-child(2) td:nth-child(3)',
    category: ['.breadcrumbs li:not(:first-child):not(:last-child)'],
    key_features: ['.product.attribute.overview'],
    // color: '.nav.features li:last-child .h-content',
    // hardware: '.extra-dts-panel > .row:nth-child(2) > div:nth-child(2) a:not([href])',

    description: '.product.attribute.description@html',
    specifications: x('.additional-attributes-wrapper.table-wrapper tbody tr', [{
        key: '.col.label',
        value: '.col.data'
    }]),
    // variation: x('.alternative-slider .slide', [{
    //     product_id: '',
    //     value: 'img@title',
    //     link: 'a@href',
    // }])
};
infoConfig.postProcess = {
    link: (_, url) => url,
    sku: data => data["sku"].split('\n')[5].replace(/"/g, '').split(':')[1],
    image_gallery: data => JSON.parse(data["temp_image_gallery"][7])['[data-gallery-role=gallery-placeholder]']['Xumulus_FastGalleryLoad/js/gallery/custom_gallery']['data'].map(d => d.img),
    temp_image_gallery: () => undefined,
    id: data => sha256(data["link"])
    

};

module.exports = infoConfig;